import 'package:flutter/material.dart';
import 'package:retalk/screens/login_screen/login_pallete.dart';

Container buildChatComposer({
  TextEditingController? msgCo,
  Function()? sendTap,
  Function()? choiceTap,
}) {
  return Container(
    padding: EdgeInsets.symmetric(horizontal: 20),
    color: Palette.backgroundColor,
    height: 100,
    child: Row(
      children: [
        Expanded(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 14),
            height: 60,
            decoration: BoxDecoration(
              color: Palette.mainColor.withOpacity(0.2),
              borderRadius: BorderRadius.circular(30),
            ),
            child: Row(
              children: [
                // Icon(
                //   Icons.emoji_emotions_outlined,
                //   color: Colors.grey[500],
                // ),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: TextField(
                    controller: msgCo,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: 'Type your message ...',
                      hintStyle: TextStyle(color: Colors.grey[500]),
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: choiceTap,
                  child: Icon(
                    Icons.attach_file,
                    color: Colors.grey[500],
                  ),
                )
              ],
            ),
          ),
        ),
        SizedBox(
          width: 16,
        ),
        GestureDetector(
          onTap: sendTap,
          child: CircleAvatar(
            backgroundColor: Palette.mainColor,
            radius: 25,
            child: Icon(
              Icons.send,
              color: Colors.white,
            ),
          ),
        )
      ],
    ),
  );
}

Container buildMediaTextField({
  TextEditingController? msgCo,
  Function()? sendTap,
  Function()? choiceTap,
}) {
  return Container(
    padding: EdgeInsets.symmetric(horizontal: 20),
    color: Palette.mainColor.withOpacity(0.5),
    height: 100,
    child: Row(
      children: [
        Expanded(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 14),
            height: 60,
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.circular(30),
            ),
            child: Row(
              children: [
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: TextField(
                    controller: msgCo,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: 'Type your message ...',
                      hintStyle: TextStyle(color: Colors.grey[500]),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        SizedBox(
          width: 16,
        ),
        GestureDetector(
          onTap: sendTap,
          child: CircleAvatar(
            backgroundColor: Palette.mainColor,
            radius: 25,
            child: Icon(
              Icons.send,
              color: Colors.white,
            ),
          ),
        )
      ],
    ),
  );
}
