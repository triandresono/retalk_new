import 'package:flutter/material.dart';
import 'package:retalk/screens/login_screen/login_pallete.dart';

class MyTabBar extends StatelessWidget {
  const MyTabBar({
    Key? key,
    required this.tabController,
  }) : super(key: key);

  final TabController tabController;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 0),
      padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 20),
      height: 80,
      color: Palette.mainColor,
      child: TabBar(
        controller: tabController,
        physics: NeverScrollableScrollPhysics(),
        isScrollable: false,
        indicatorColor: Colors.transparent,
        unselectedLabelColor: Palette.backgroundColor,
        indicator: BoxDecoration(
          color: Palette.backgroundColor,
          borderRadius: BorderRadius.circular(25),
        ),
        labelColor: Palette.mainColor,
        tabs: [
          Tab(
            icon: Text(
              'Chats',
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w300,
              ),
            ),
          ),
          Tab(
            icon: Text(
              'Status',
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w300,
              ),
            ),
          ),
          // Tab(
          //   icon: Text(
          //     'Add friends',
          //     style: TextStyle(
          //       fontSize: 18,
          //       fontWeight: FontWeight.w300,
          //     ),
          //   ),
          // ),
        ],
      ),
    );
  }
}
