import 'dart:io';
import 'dart:typed_data';
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:photo_manager/photo_manager.dart';
import 'package:retalk/screens/helper/matrix_color.dart';
import 'package:retalk/screens/login_screen/login_pallete.dart';
import 'package:retalk/service/locator.dart';
import 'package:retalk/service/navigator_service.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';

class CustomMediaPicker extends StatefulWidget {
  final bool? enableMulti;
  final bool? enableVideo;
  CustomMediaPicker({this.enableMulti, this.enableVideo});
  @override
  _CustomMediaPickerState createState() => _CustomMediaPickerState();
}

class _CustomMediaPickerState extends State<CustomMediaPicker> {
  List<Widget> _mediaList = [];
  List<AssetPathEntity> albums = [];
  List<Uint8List?> thumb = [];
  List<AssetEntity> unSelectedData = [];
  List<AssetEntity> selectedData = [];
  List<List<int>> selectedIndex = [];
  List<int?> single = [];
  int currentPage = 0;
  int selectedAlbum = 0;
  int? selectedImage;
  int? lastPage;
  bool? enableMulti;
  bool? enableVideo;
  @override
  void initState() {
    super.initState();
    enableMulti = widget.enableMulti ?? true;
    enableVideo = widget.enableVideo ?? false;
    fetchAlbum().then((_) {
      _fetchNewMedia();
    });
  }

  _handleScrollEvent(ScrollNotification scroll) {
    if (scroll.metrics.pixels / scroll.metrics.maxScrollExtent > 0.33) {
      if (currentPage != lastPage) {
        _fetchNewMedia();
      }
    }
  }

  Future<void> fetchAlbum() async {
    albums = await PhotoManager.getAssetPathList(onlyAll: false);
    for (int i = 0; i < albums.length; i++) {
      await albums[i].getAssetListPaged(0, 1).then((value) async {
        if (value.isNotEmpty) {
          Uint8List? file =
              await value[0].thumbDataWithSize(200, 200, quality: 50);
          selectedIndex.add([]);
          single.add(null);
          thumb.add(file);
        }
      });
    }
  }

  _fetchNewMedia() async {
    List<Widget> temp = [];
    List<AssetEntity> tempData = [];
    lastPage = currentPage;

    List<AssetEntity> media = await albums[selectedAlbum].getAssetListPaged(
      currentPage,
      60,
    );

    if (!enableVideo!) {
      for (var asset in media) {
        if (asset.type != AssetType.video) {
          tempData.add(asset);
          temp.add(
            FutureBuilder<Uint8List?>(
              future: asset.thumbDataWithSize(200, 200, quality: 50),
              builder: (BuildContext context, snapshot) {
                if (snapshot.hasData) {
                  return Image.memory(
                    snapshot.data!,
                    fit: BoxFit.cover,
                  );
                } else {
                  return Container();
                }
              },
            ),
          );
        }
      }
    } else {
      for (var asset in media) {
        if (asset.type != AssetType.image) {
          tempData.add(asset);
          temp.add(
            FutureBuilder<Uint8List?>(
              future: asset.thumbDataWithSize(200, 200, quality: 50),
              builder: (BuildContext context, snapshot) {
                if (snapshot.hasData) {
                  return Image.memory(
                    snapshot.data!,
                    fit: BoxFit.cover,
                  );
                } else {
                  return Container();
                }
              },
            ),
          );
        }
      }
    }

    setState(() {
      _mediaList.addAll(temp);
      unSelectedData.addAll(tempData);
      currentPage++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 3,
        backgroundColor: Palette.mainColor,
        titleSpacing: 5,
        automaticallyImplyLeading: false,
        title: Stack(
          children: <Widget>[
            Row(
              children: [
                GestureDetector(
                  onTap: () => locator<NavigatorService>().goBack(),
                  child: Container(
                    margin: EdgeInsets.only(right: 15),
                    height: 40,
                    width: 40,
                    child: Center(
                      child: Icon(Icons.arrow_back),
                    ),
                  ),
                ),
                AnimatedSwitcher(
                  duration: Duration(milliseconds: 300),
                  transitionBuilder: (a, b) => ScaleTransition(
                    child: a,
                    scale: b,
                  ),
                  child: (albums.isNotEmpty)
                      ? GestureDetector(
                          onTap: () {
                            showAlbum(albums, thumb, context,
                                    selected: selectedIndex)
                                .then((int? val) {
                              if (val != null) {
                                setState(() {
                                  selectedAlbum = val;
                                  currentPage = 0;
                                  _mediaList.clear();
                                  unSelectedData.clear();
                                  _fetchNewMedia();
                                });
                              }
                            });
                          },
                          child: Row(
                            children: [
                              Text(
                                albums[selectedAlbum].name,
                                style: TextStyle(
                                  color: Palette.backgroundColor,
                                  fontWeight: FontWeight.w300,
                                  fontSize: 20,
                                ),
                              ),
                              RotatedBox(
                                quarterTurns: 1,
                                child: Padding(
                                  padding: const EdgeInsets.only(top: 3.0),
                                  child: Icon(
                                    Icons.chevron_right,
                                    color: Palette.backgroundColor,
                                    size: 30,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )
                      : SizedBox(
                          height: 40,
                          width: 40,
                          child: Center(child: CupertinoActivityIndicator()),
                        ),
                )
              ],
            ),
            Container(
              height: 45,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  GestureDetector(
                    behavior: HitTestBehavior.opaque,
                    onTap: () async {
                      if (enableMulti!) {
                        List<File> result = [];
                        for (int i = 0; i < selectedData.length; i++) {
                          File? data = await selectedData[i].file;
                          result.add(data!);
                        }
                        locator<NavigatorService>().goBack(result);
                      } else {
                        File? file = await selectedData[0].file;
                        File? result = await compressFile(file!);
                        ImageClass data = determineRatio(
                          selectedData[0].width,
                          selectedData[0].height,
                          result!,
                        );
                        locator<NavigatorService>().goBack(data);
                      }
                    },
                    child: Center(
                      child: Container(
                        padding: const EdgeInsets.only(right: 20, left: 10),
                        child: Text(
                          'Done',
                          style: TextStyle(
                            fontSize: 20,
                            fontFamily: 'AvertaPE',
                            fontWeight: FontWeight.w300,
                            color: Palette.backgroundColor,
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
      body: NotificationListener<ScrollNotification>(
        onNotification: (ScrollNotification scroll) {
          _handleScrollEvent(scroll);
          return false;
        },
        child: (_mediaList.isNotEmpty)
            ? Container(
                child: (enableMulti!)
                    ? GridView.builder(
                        physics: BouncingScrollPhysics(),
                        itemCount: _mediaList.length,
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 3,
                          crossAxisSpacing: 4,
                          mainAxisSpacing: 4,
                        ),
                        itemBuilder: (BuildContext context, int index) {
                          return GridItem(
                            key: Key((index + 1).toString()),
                            child: _mediaList[index],
                            check: checking(index),
                            isSelected: (res) {
                              setState(() {
                                if (res) {
                                  selectedData.add(unSelectedData[index]);
                                  selectedIndex[selectedAlbum].add(index);
                                } else {
                                  selectedData.remove(unSelectedData[index]);
                                  selectedIndex[selectedAlbum].remove(index);
                                }
                              });
                            },
                          );
                        },
                      )
                    : (!enableVideo!)
                        ? GridView.builder(
                            physics: BouncingScrollPhysics(),
                            itemCount: _mediaList.length,
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 3,
                              crossAxisSpacing: 4,
                              mainAxisSpacing: 4,
                            ),
                            itemBuilder: (BuildContext context, int index) {
                              return GestureDetector(
                                onTap: () {
                                  setState(() {
                                    if (single[selectedAlbum] == index) {
                                      single[selectedAlbum] = null;
                                      selectedData
                                          .remove(unSelectedData[index]);
                                    } else {
                                      if (single[selectedAlbum] != null) {
                                        int x = single[selectedAlbum]!;
                                        single[selectedAlbum] = index;
                                        selectedData.add(unSelectedData[index]);
                                        selectedData.remove(unSelectedData[x]);
                                      } else {
                                        single[selectedAlbum] = index;
                                        selectedData.add(unSelectedData[index]);
                                      }
                                    }
                                  });
                                },
                                child: GridSinglePick(
                                  child: _mediaList[index],
                                  isSelected: singleCheck(index),
                                ),
                              );
                            },
                          )
                        : GridView.builder(
                            physics: BouncingScrollPhysics(),
                            itemCount: _mediaList.length,
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 3,
                              crossAxisSpacing: 4,
                              mainAxisSpacing: 4,
                            ),
                            itemBuilder: (BuildContext context, int index) {
                              return GestureDetector(
                                onTap: () {
                                  setState(() {
                                    if (single[selectedAlbum] == index) {
                                      single[selectedAlbum] = null;
                                      selectedData
                                          .remove(unSelectedData[index]);
                                    } else {
                                      if (single[selectedAlbum] != null) {
                                        int x = single[selectedAlbum]!;
                                        single[selectedAlbum] = index;
                                        selectedData.add(unSelectedData[index]);
                                        selectedData.remove(unSelectedData[x]);
                                      } else {
                                        single[selectedAlbum] = index;
                                        selectedData.add(unSelectedData[index]);
                                      }
                                    }
                                  });
                                },
                                child: GridVideoPick(
                                  child: _mediaList[index],
                                  isSelected: singleCheck(index),
                                  duration: formatDuration(Duration(
                                      seconds: unSelectedData[index].duration)),
                                ),
                              );
                            },
                          ),
              )
            : Center(
                child: Container(
                child: Text(
                  (!enableVideo!)
                      ? 'There are no image file on this album'
                      : 'There are no video file on this album',
                ),
              )),
      ),
    );
  }

  Future<int> showAlbum(
    List<AssetPathEntity> album,
    List<Uint8List?> image,
    BuildContext context, {
    List<int>? singleSelect,
    List<List<int>>? selected,
  }) async {
    return showModalBottomSheet(
      context: context,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(top: Radius.circular(20)),
      ),
      // isScrollControlled: true,
      builder: (context) {
        return Container(
          padding: const EdgeInsets.only(top: 50, bottom: 30),
          child: ListView.separated(
            separatorBuilder: (context, index) {
              return Divider();
            },
            itemCount: album.length,
            itemBuilder: (context, index) {
              return ListTile(
                title: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(album[index].name),
                    SizedBox(height: 15),
                    Text(album[index].assetCount.toString()),
                  ],
                ),
                trailing: Text(selected![index].length > 0
                    ? selected[index].length.toString()
                    : ''),
                leading: Container(
                  height: 60,
                  width: 60,
                  child: Image.memory(
                    image[index]!,
                    fit: BoxFit.cover,
                  ),
                ),
                onTap: () {
                  locator<NavigatorService>().goBack(index);
                },
              );
            },
          ),
        );
      },
    ).then((res) {
      return res;
    });
  }

  bool checking(int i) {
    return selectedIndex[selectedAlbum].isNotEmpty &&
        selectedIndex[selectedAlbum].contains(i);
  }

  bool singleCheck(int i) {
    return single[selectedAlbum] != null && single[selectedAlbum] == i;
  }

  Future<File?> compressFile(File file) async {
    final filePath = file.absolute.path;
    final lastIndex = filePath.lastIndexOf(new RegExp(r'.jp'));
    final splitted = filePath.substring(0, (lastIndex));
    final outPath = "${splitted}_out${filePath.substring(lastIndex)}";
    var result = await FlutterImageCompress.compressAndGetFile(
      file.absolute.path,
      outPath,
      quality: 65,
    );

    return result;
  }
}

class GridItem extends StatefulWidget {
  final Key? key;
  final Function(bool)? isSelected;
  final bool? check;
  final Widget? child;

  GridItem({this.isSelected, this.key, this.check, this.child});

  @override
  _GridItemState createState() => _GridItemState();
}

class _GridItemState extends State<GridItem> {
  bool isSelected = false;

  @override
  void initState() {
    super.initState();
    if (widget.check != null && widget.check!) {
      isSelected = true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        setState(() {
          isSelected = !isSelected;
          widget.isSelected!(isSelected);
        });
      },
      child: AnimatedPadding(
        duration: Duration(milliseconds: 200),
        padding: EdgeInsets.all(isSelected ? 10 : 0),
        child: Stack(
          children: <Widget>[
            Positioned.fill(
              child: ColorFiltered(
                colorFilter: ColorFilter.matrix(
                  isSelected
                      ? MatrixColor.selectedFilter
                      : MatrixColor.nofilter,
                ),
                child: widget.child,
              ),
            ),
            isSelected
                ? Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Align(
                      alignment: Alignment.bottomRight,
                      child: Container(
                        decoration: BoxDecoration(
                          color: Color(0xff2DB6BB),
                          shape: BoxShape.circle,
                        ),
                        padding: const EdgeInsets.all(4.0),
                        child: Icon(
                          Icons.check,
                          color: Colors.white,
                          size: 20,
                        ),
                      ),
                    ),
                  )
                : Container()
          ],
        ),
      ),
    );
  }
}

class GridSinglePick extends StatelessWidget {
  final Widget? child;
  final bool? isSelected;
  GridSinglePick({this.child, this.isSelected});
  @override
  Widget build(BuildContext context) {
    return AnimatedPadding(
      padding: EdgeInsets.all(isSelected! ? 10 : 0),
      duration: Duration(milliseconds: 200),
      child: Stack(
        children: <Widget>[
          Positioned.fill(
            child: ColorFiltered(
              colorFilter: ColorFilter.matrix(
                isSelected! ? MatrixColor.selectedFilter : MatrixColor.nofilter,
              ),
              child: child,
            ),
          ),
          isSelected!
              ? Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Align(
                    alignment: Alignment.bottomRight,
                    child: Container(
                      decoration: BoxDecoration(
                        color: Palette.mainColor,
                        shape: BoxShape.circle,
                      ),
                      padding: const EdgeInsets.all(4.0),
                      child: Icon(
                        Icons.check,
                        color: Colors.white,
                        size: 20,
                      ),
                    ),
                  ),
                )
              : Container()
        ],
      ),
    );
  }
}

class GridVideoPick extends StatelessWidget {
  final Widget? child;
  final bool? isSelected;
  final String? duration;
  GridVideoPick({this.child, this.isSelected, this.duration});
  @override
  Widget build(BuildContext context) {
    return AnimatedPadding(
      padding: EdgeInsets.all(isSelected! ? 10 : 0),
      duration: Duration(milliseconds: 200),
      child: Stack(
        children: <Widget>[
          Positioned.fill(
            child: ColorFiltered(
              colorFilter: ColorFilter.matrix(
                isSelected! ? MatrixColor.selectedFilter : MatrixColor.nofilter,
              ),
              child: child,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Align(
              alignment: Alignment.topRight,
              child: ClipRRect(
                child: Container(
                  padding: const EdgeInsets.all(4.0),
                  child: BackdropFilter(
                      filter: ImageFilter.blur(sigmaX: 4.0, sigmaY: 4.0),
                      child: Text(
                        duration!,
                        style: TextStyle(color: Colors.white),
                      )),
                ),
              ),
            ),
          ),
          isSelected!
              ? Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Align(
                    alignment: Alignment.bottomRight,
                    child: Container(
                      decoration: BoxDecoration(
                        color: Color(0xff2DB6BB),
                        shape: BoxShape.circle,
                      ),
                      padding: const EdgeInsets.all(4.0),
                      child: Icon(
                        Icons.check,
                        color: Colors.white,
                        size: 20,
                      ),
                    ),
                  ),
                )
              : Container()
        ],
      ),
    );
  }
}

String formatDuration(Duration d) {
  String twoDigits(int n) => n.toString().padLeft(2, "0");
  String twoDigitMinutes = twoDigits(d.inMinutes.remainder(60));
  String twoDigitSeconds = twoDigits(d.inSeconds.remainder(60));
  return (d.inHours != 0)
      ? "${twoDigits(d.inHours)}:$twoDigitMinutes:$twoDigitSeconds"
      : "$twoDigitMinutes:$twoDigitSeconds";
}

class ImageClass {
  File? file;
  double? aspectRatio;

  ImageClass({this.file, this.aspectRatio});
}

class CropAspectRatios {
  /// no aspect ratio for crop
  // static const double custom = null;

  /// the same as aspect ratio of image
  /// [cropAspectRatio] is not more than 0.0, it's original
  static const double original = 0.0;

  /// ratio of width and height is 1 : 1
  static const double ratio1_1 = 1.0;

  /// ratio of width and height is 3 : 4
  static const double ratio3_4 = 3.0 / 4.0;

  /// ratio of width and height is 4 : 3
  static const double ratio4_3 = 4.0 / 3.0;

  /// ratio of width and height is 4 : 4
  static const double ratio4_4 = 4.0 / 4.0;

  /// ratio of width and height is 9 : 16
  static const double ratio9_16 = 9.0 / 16.0;

  /// ratio of width and height is 16 : 9
  static const double ratio16_9 = 16.0 / 9.0;
}

ImageClass determineRatio(int width, int height, File file) {
  double cropRatio = 0.0;
  ImageClass res = new ImageClass();
  if (height >= width && height - width <= 30) {
    //Square
    cropRatio = CropAspectRatios.ratio1_1;
  } else if (width >= height && width - height <= 30) {
    //Square
    cropRatio = CropAspectRatios.ratio1_1;
  } else if (height > width) {
    //Potrait
    cropRatio = CropAspectRatios.ratio3_4;
  } else if (height < width) {
    //Landscape
    cropRatio = CropAspectRatios.ratio4_3;
  }

  res.aspectRatio = cropRatio;
  res.file = file;
  return res;
}
