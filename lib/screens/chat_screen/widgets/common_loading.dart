import 'package:flutter/material.dart';
import 'package:retalk/screens/login_screen/login_pallete.dart';

showloadingDialog(BuildContext context) {
  showDialog(
      context: context,
      barrierColor: Color(0x00ffffff),
      builder: (_) {
        return Center(
          child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation(Palette.mainColor),
          ),
        );
      });
}
