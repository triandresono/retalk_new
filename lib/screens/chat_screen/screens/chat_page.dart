import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:retalk/bloc/chat_page_bloc/chat_page_bloc.dart';
import 'package:retalk/bloc/chat_page_bloc/chat_page_event.dart';
import 'package:retalk/bloc/chat_page_bloc/chat_page_state.dart';
import 'package:retalk/models/friends.dart';
import 'package:retalk/screens/helper/constants.dart';
import 'package:retalk/screens/login_screen/login_pallete.dart';
import 'package:flutter/material.dart';
import 'package:retalk/screens/widgets/profile_image.dart';
import 'package:retalk/service/locator.dart';
import 'package:retalk/service/navigator_service.dart';

import '../app_theme.dart';

class ChatPage extends StatefulWidget {
  const ChatPage({
    Key? key,
  }) : super(key: key);

  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  Timer? searchOnStoppedTyping;
  Stream<QuerySnapshot>? peerList;
  TextEditingController searchCo = new TextEditingController();
  @override
  void initState() {
    super.initState();
    bloc(GetChatPage(''));
  }

  bloc(dynamic event) {
    BlocProvider.of<ChatPageBloc>(context).add(event);
  }

  onChangeHandler(value) {
    const duration = Duration(milliseconds: 800);
    if (searchOnStoppedTyping != null) {
      setState(() => searchOnStoppedTyping!.cancel());
    }
    setState(() => searchOnStoppedTyping =
        new Timer(duration, () => bloc(GetChatPage(value.toLowerCase()))));
  }

  chatFriends(DocumentSnapshot data) {
    List<String> nameSearch = [];
    List<String> emailSearch = [];
    FriendsList added = new FriendsList();
    String roomId = data[FireStoreKey.chatRoomId];
    added.aboutMe = data[FireStoreKey.aboutMe];
    added.email = data[FireStoreKey.email];
    added.imageUrl = data[FireStoreKey.image];
    added.userId = data[FireStoreKey.userId];
    added.userName = data[FireStoreKey.userName];
    added.documentId = data[FireStoreKey.documentId];
    added.chatRoomId = roomId;
    List nameRes = data[FireStoreKey.nameSearch];
    List emailRes = data[FireStoreKey.emailSearch];
    nameRes.forEach((a) {
      nameSearch.add(a);
    });
    emailRes.forEach((b) {
      emailSearch.add(b);
    });
    added.nameSearch = nameSearch;
    added.emailSearch = emailSearch;

    bloc(GotoChatPage(roomId, added));
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<ChatPageBloc, ChatPageState>(
      listener: (context, state) {
        if (state is GetChatPageComplete) {
          setState(() {
            peerList = state.stream;
          });
        }
        if (state is GoChatComplete) {
          locator<NavigatorService>()
              .navigateToWithArgmnt(PageConstants.CHAT_ROOM, state.result);
        }
      },
      child: Container(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Column(
                children: [
                  Container(
                    padding: EdgeInsets.only(top: 10),
                    child: Row(
                      children: [
                        Expanded(
                          child: Container(
                            child: TextFormField(
                              cursorColor: Palette.mainColor,
                              decoration: searchDecor(),
                              controller: searchCo,
                              onChanged: (val) => onChangeHandler(val),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  StreamBuilder(
                    stream: peerList,
                    builder: (context, AsyncSnapshot snapshot) {
                      if (snapshot.hasData) {
                        if (snapshot.data!.docs.length > 0) {
                          return ListView.builder(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 20),
                              shrinkWrap: true,
                              physics: ScrollPhysics(),
                              itemCount: snapshot.data.docs.length,
                              itemBuilder: (context, index) {
                                DocumentSnapshot query =
                                    snapshot.data.docs[index];
                                return AnimationConfiguration.staggeredList(
                                  position: index,
                                  duration: const Duration(milliseconds: 375),
                                  child: SlideAnimation(
                                    verticalOffset: 50.0,
                                    child: FadeInAnimation(
                                      child: tileList(query),
                                    ),
                                  ),
                                );
                              });
                        } else {
                          return Padding(
                            padding: const EdgeInsets.only(top: 200.0),
                            child: GestureDetector(
                              onTap: () => getFriendList(),
                              child: Container(
                                decoration: BoxDecoration(
                                    color: Palette.mainColor,
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(25),
                                      topRight: Radius.circular(25),
                                      bottomLeft: Radius.circular(0),
                                      bottomRight: Radius.circular(12),
                                    )),
                                child: Padding(
                                  padding: const EdgeInsets.all(15.0),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Icon(
                                        Icons.message,
                                        color: Palette.backgroundColor,
                                      ),
                                      SizedBox(height: 10),
                                      Text(
                                        'You have no chats...',
                                        style: TextStyle(
                                          color: Palette.backgroundColor,
                                          fontWeight: FontWeight.w300,
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          );
                        }
                      } else {
                        return Padding(
                          padding: const EdgeInsets.only(top: 200.0),
                          child: Center(
                            child: CircularProgressIndicator(
                              strokeWidth: 3.5,
                              valueColor: AlwaysStoppedAnimation(
                                Palette.mainColor,
                              ),
                            ),
                          ),
                        );
                      }
                    },
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget tileList(DocumentSnapshot query) {
    Map<String, dynamic> map = query.data() as Map<String, dynamic>;
    return GestureDetector(
      onTap: () => chatFriends(query),
      child: PeerList(
        aboutMe: map[FireStoreKey.aboutMe],
        profileName: map[FireStoreKey.userName],
        profileUrl: map[FireStoreKey.image],
        imagesContent: map[FireStoreKey.imagesContent],
        lastChat: map[FireStoreKey.lastChat] ?? '',
        lastChatType: map[FireStoreKey.lastChatType] ?? '',
        bodyTap: () => chatFriends(query),
        addTap: () => chatFriends(query),
      ),
    );
  }

  getFriendList() {
    locator<NavigatorService>().navigateTo(PageConstants.FRIEND_LIST);
  }
}

class PeerList extends StatelessWidget {
  final String? profileUrl;
  final String? profileName;
  final String? aboutMe;
  final Function()? addTap;
  final Function()? bodyTap;
  final String? imagesContent;
  final String? lastChat;
  final String? lastChatType;
  PeerList({
    Key? key,
    this.aboutMe,
    this.addTap,
    this.bodyTap,
    this.profileName,
    this.profileUrl,
    this.lastChat,
    this.lastChatType,
    this.imagesContent,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 20),
      color: Palette.backgroundColor,
      child: Row(
        children: [
          CircleWithErrorBuilder(
            size: 28,
            imgProfileUrl: profileUrl!,
          ),
          SizedBox(
            width: 10,
          ),
          Expanded(
            child: GestureDetector(
              onTap: bodyTap,
              child: Container(
                // color: Palette.mainColor,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      profileName!,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: MyTheme.heading2.copyWith(
                        fontSize: 16,
                      ),
                    ),
                    SizedBox(height: 5),
                    (lastChatType == Constants.type_normal)
                        ? Text(
                            (lastChat!.isEmpty) ? aboutMe! : lastChat!,
                            style: MyTheme.bodyText1,
                            overflow: TextOverflow.ellipsis,
                          )
                        : Row(
                            children: [
                              Icon(Icons.camera_alt, color: Palette.mainColor),
                              SizedBox(width: 10),
                              Expanded(
                                child: Text(
                                  (lastChat!.isEmpty)
                                      ? imagesContent!
                                      : lastChat!,
                                  style: MyTheme.bodyText1,
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 1,
                                  softWrap: true,
                                ),
                              )
                            ],
                          ),
                  ],
                ),
              ),
            ),
          ),
          GestureDetector(
            onTap: addTap,
            child: Container(
              padding: const EdgeInsets.only(left: 15, top: 10, bottom: 10),
              child: Icon(
                Icons.message_outlined,
                color: Palette.mainColor,
              ),
            ),
          )
        ],
      ),
    );
  }
}
