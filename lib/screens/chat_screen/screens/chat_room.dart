import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_blurhash/flutter_blurhash.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:retalk/bloc/chat_room_bloc/chat_room_bloc.dart';
import 'package:retalk/bloc/chat_room_bloc/chat_room_event.dart';
import 'package:retalk/bloc/chat_room_bloc/chat_room_state.dart';
import 'package:retalk/bloc/media_message_bloc/media_message_bloc.dart';
import 'package:retalk/models/friends.dart';
import 'package:retalk/models/users.dart';
import 'package:retalk/screens/chat_screen/models/chat_model.dart';
import 'package:retalk/screens/chat_screen/screens/media_message.dart';
import 'package:retalk/screens/chat_screen/widgets/custom_media_picker.dart';
import 'package:retalk/screens/helper/constants.dart';
import 'package:retalk/screens/login_screen/login_pallete.dart';
import 'package:intl/intl.dart';
import 'package:retalk/service/firebase_instance.dart';
import 'package:retalk/service/locator.dart';
import 'package:retalk/service/navigator_service.dart';

import '../app_theme.dart';
import 'package:flutter/material.dart';
import '../models/user_model.dart';
import '../widgets/widgets.dart';

class ChatRoom extends StatefulWidget {
  const ChatRoom({Key? key, this.user, this.data}) : super(key: key);

  @override
  _ChatRoomState createState() => _ChatRoomState();
  final User? user;
  final FriendsList? data;
}

class _ChatRoomState extends State<ChatRoom> {
  TextEditingController msgCo = new TextEditingController();
  CloudFirestoreInstance fr = new CloudFirestoreInstance();
  AsyncSnapshot<dynamic>? data;
  Stream<QuerySnapshot>? chatStream;
  Stream<DocumentSnapshot>? onlineStream;
  Users? user;
  String lastChat = '';
  @override
  void initState() {
    bloc(GetChatEvent(
      roomId: widget.data!.chatRoomId!,
      friendName: widget.data!.userName!,
    ));
    bloc(GetOnlineStatus(
      roomId: widget.data!.chatRoomId!,
      friendId: widget.data!.userId!,
    ));
    super.initState();
  }

  bloc(dynamic event) {
    BlocProvider.of<ChatRoomBloc>(context).add(event);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        await fr.updateInRoom(widget.data!.chatRoomId, user!.userId, false);
        return Future.value(true);
      },
      child: BlocListener<ChatRoomBloc, ChatRoomState>(
        listener: (context, state) {
          if (state is GetChatComplete) {
            setState(() {
              chatStream = state.result;
              user = state.user;
            });
          }
          if (state is GetOnlineComplete) {
            setState(() {
              onlineStream = state.result;
            });
          }
          if (state is AddChatComplete) {
            setState(() {
              msgCo.clear();
            });
          }
        },
        child: Scaffold(
          backgroundColor: Palette.mainColor,
          // resizeToAvoidBottomPadding: false,
          appBar: AppBar(
            backgroundColor: Palette.mainColor,
            toolbarHeight: 100,
            centerTitle: false,
            title: Row(
              children: [
                CircleAvatar(
                  radius: 30,
                  backgroundImage: CachedNetworkImageProvider(
                    widget.data!.imageUrl!,
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        widget.data!.userName!,
                        overflow: TextOverflow.ellipsis,
                        style: MyTheme.chatSenderName,
                      ),
                      Text(
                        'online',
                        style: MyTheme.bodyText1.copyWith(fontSize: 18),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            actions: [
              Container(
                padding: const EdgeInsets.only(right: 20),
                child: Icon(
                  Icons.person,
                  size: 28,
                ),
              ),
            ],
            elevation: 0,
          ),
          body: GestureDetector(
            onTap: () {
              FocusScope.of(context).unfocus();
            },
            child: Column(
              children: [
                Expanded(
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    decoration: BoxDecoration(
                      color: Palette.backgroundColor,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30),
                        topRight: Radius.circular(30),
                      ),
                    ),
                    child: StreamBuilder<DocumentSnapshot>(
                      stream: onlineStream,
                      builder: (context, snap) {
                        if (snap.hasData) {
                          data = snap;
                          return StreamBuilder<QuerySnapshot>(
                            stream: chatStream,
                            builder: (context, snapshot2) {
                              if (snapshot2.hasData) {
                                return ListView.builder(
                                    reverse: true,
                                    itemCount: snapshot2.data!.docs.length,
                                    itemBuilder: (context, int index) {
                                      DocumentSnapshot query =
                                          snapshot2.data!.docs[index];
                                      return AnimationConfiguration
                                          .staggeredList(
                                        position: index,
                                        duration:
                                            const Duration(milliseconds: 375),
                                        child: SlideAnimation(
                                          verticalOffset: 50.0,
                                          child: FadeInAnimation(
                                            child: tile(query, snap),
                                          ),
                                        ),
                                      );
                                    });
                              } else {
                                return Center(
                                  child: CircularProgressIndicator(
                                    strokeWidth: 3.5,
                                    valueColor: AlwaysStoppedAnimation(
                                      Palette.mainColor,
                                    ),
                                  ),
                                );
                              }
                            },
                          );
                        } else {
                          return Container();
                        }
                      },
                    ),
                  ),
                ),
                buildChatComposer(
                    msgCo: msgCo,
                    choiceTap: () {
                      FocusScope.of(context).unfocus();
                      uploadChoice().then((ImageClass? val) async {
                        if (val != null) {
                          MediaClass? message = await Navigator.push(
                            context,
                            CupertinoPageRoute(
                              builder: (context) =>
                                  BlocProvider<MediaMessageBloc>(
                                create: (context) => MediaMessageBloc(),
                                child: MediaMessage(
                                  image: val.file,
                                  imageUrl: widget.data!.imageUrl!,
                                  userName: widget.data!.userName!,
                                  aspectRatio: val.aspectRatio,
                                ),
                              ),
                            ),
                          );
                          print(message);
                          if (message != null) {
                            ChatModel chat = new ChatModel();
                            chat.message = message.mediaCOntent;
                            chat.imagesContent = message.imageUrl;
                            chat.sendBy = user!.userName;
                            chat.type = Constants.type_image;
                            chat.time = DateTime.now().millisecondsSinceEpoch;
                            chat.isRead = data!.data[FireStoreKey.isInRoom];
                            chat.hash = message.blurHash;

                            bloc(AddChatEvent(
                              chatRoomId: widget.data!.chatRoomId!,
                              model: chat,
                              friend: widget.data!,
                            ));
                          }
                        }
                      });
                    },
                    sendTap: () {
                      if (msgCo.text.isNotEmpty) {
                        ChatModel model = new ChatModel();
                        model.message = msgCo.text;
                        model.sendBy = user!.userName;
                        model.type = Constants.type_normal;
                        model.time = DateTime.now().millisecondsSinceEpoch;
                        model.isRead = data!.data[FireStoreKey.isInRoom];

                        bloc(AddChatEvent(
                          chatRoomId: widget.data!.chatRoomId!,
                          model: model,
                          friend: widget.data!,
                        ));
                      }
                    })
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget tile(DocumentSnapshot query, AsyncSnapshot<dynamic> snap) {
    if (query[FireStoreKey.type] == Constants.type_image) {
      return ImageChatTile(
        isInRoom: snap.data[FireStoreKey.isInRoom],
        isRead: query[FireStoreKey.isRead],
        isMe: query[FireStoreKey.sendBy] == user!.userName,
        imageUrl: user!.imageUrl!,
        message: query[FireStoreKey.message],
        time: query[FireStoreKey.chatTime],
        messageUrl: query[FireStoreKey.imagesContent],
        hash: query[FireStoreKey.hash],
      );
    } else {
      return ChatTile(
        isInRoom: snap.data[FireStoreKey.isInRoom],
        isRead: query[FireStoreKey.isRead],
        isMe: query[FireStoreKey.sendBy] == user!.userName,
        imageUrl: user!.imageUrl!,
        message: query[FireStoreKey.message],
        time: query[FireStoreKey.chatTime],
      );
    }
  }

  Future<ImageClass> uploadChoice() {
    return showModalBottomSheet(
        context: context,
        backgroundColor: Palette.backgroundColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(20)),
        ),
        builder: (context) {
          return UploadChoice(
            onTapMedia: () async {
              ImageClass? image = await Navigator.push(
                context,
                CupertinoPageRoute(
                  builder: (context) => CustomMediaPicker(
                    enableMulti: false,
                  ),
                ),
              );
              if (image != null) {
                locator<NavigatorService>().goBack(image);
              }
            },
          );
        }).then((val) {
      return val;
    });
  }
}

class UploadChoice extends StatelessWidget {
  final Function()? onTapMedia;
  const UploadChoice({Key? key, this.onTapMedia}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView(
        shrinkWrap: true,
        children: [
          GestureDetector(
            onTap: onTapMedia,
            child: UploadWidget(
              text: 'Media',
              icon: Icon(
                Icons.image,
                color: Palette.backgroundColor,
                size: 30,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class UploadWidget extends StatelessWidget {
  final Widget? icon;
  final String? text;
  final Function()? onTao;
  UploadWidget({Key? key, this.icon, this.text, this.onTao}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 20, bottom: 20),
      padding: const EdgeInsets.symmetric(horizontal: 20),
      color: Palette.backgroundColor,
      child: Row(
        children: [
          CircleAvatar(
            radius: 28,
            child: icon,
            backgroundColor: Palette.mainColor,
          ),
          SizedBox(
            width: 20,
          ),
          GestureDetector(
            onTap: () {},
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  text!,
                  style: MyTheme.heading2.copyWith(
                    fontSize: 16,
                  ),
                ),
              ],
            ),
          ),
          Spacer(),
        ],
      ),
    );
  }
}

class ChatTile extends StatelessWidget {
  final bool? isInRoom;
  final bool? isMe;
  final bool? isRead;
  final String? imageUrl;
  final String? message;
  final int? time;
  ChatTile({
    Key? key,
    this.isRead,
    this.time,
    this.isMe,
    this.imageUrl,
    this.message,
    this.isInRoom,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 10),
      child: Column(
        children: [
          Row(
            mainAxisAlignment:
                isMe! ? MainAxisAlignment.end : MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              if (!isMe!)
                CircleAvatar(
                  radius: 15,
                  backgroundImage: CachedNetworkImageProvider(imageUrl!),
                ),
              SizedBox(
                width: 10,
              ),
              Container(
                padding: EdgeInsets.all(10),
                constraints: BoxConstraints(
                    maxWidth: MediaQuery.of(context).size.width * 0.6),
                decoration: BoxDecoration(
                    color: isMe!
                        ? Palette.mainColor
                        : Palette.mainColor.withOpacity(0.2),
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(16),
                      topRight: Radius.circular(16),
                      bottomLeft: Radius.circular(isMe! ? 12 : 0),
                      bottomRight: Radius.circular(isMe! ? 0 : 12),
                    )),
                child: Text(
                  message!,
                  style: MyTheme.bodyTextMessage
                      .copyWith(color: isMe! ? Colors.white : Colors.grey[800]),
                ),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(top: 5),
            child: Row(
              mainAxisAlignment:
                  isMe! ? MainAxisAlignment.end : MainAxisAlignment.start,
              children: [
                if (!isMe!)
                  SizedBox(
                    width: 40,
                  ),
                isMe!
                    ? Icon(
                        Icons.done_all,
                        size: 20,
                        color: !isRead!
                            ? MyTheme.bodyTextTime.color
                            : Palette.mainColor,
                      )
                    : SizedBox(),
                SizedBox(
                  width: 8,
                ),
                Text(
                  readTimestamp(time!),
                  style: MyTheme.bodyTextTime,
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

class ImageChatTile extends StatelessWidget {
  final bool? isInRoom;
  final bool? isMe;
  final bool? isRead;
  final String? imageUrl;
  final String? messageUrl;
  final String? message;
  final int? time;
  final String? hash;
  ImageChatTile({
    Key? key,
    this.hash,
    this.messageUrl,
    this.isRead,
    this.time,
    this.isMe,
    this.imageUrl,
    this.message,
    this.isInRoom,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 10),
      child: Column(
        children: [
          Row(
            mainAxisAlignment:
                isMe! ? MainAxisAlignment.end : MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              if (!isMe!)
                CircleAvatar(
                  radius: 15,
                  backgroundImage: CachedNetworkImageProvider(imageUrl!),
                ),
              SizedBox(
                width: 10,
              ),
              Container(
                padding: EdgeInsets.all(10),
                constraints: BoxConstraints(
                    maxWidth: MediaQuery.of(context).size.width * 0.6),
                decoration: BoxDecoration(
                    color: isMe!
                        ? Palette.mainColor
                        : Palette.mainColor.withOpacity(0.2),
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(16),
                      topRight: Radius.circular(16),
                      bottomLeft: Radius.circular(isMe! ? 12 : 0),
                      bottomRight: Radius.circular(isMe! ? 0 : 12),
                    )),
                child: Column(
                  crossAxisAlignment: !isMe!
                      ? CrossAxisAlignment.start
                      : CrossAxisAlignment.end,
                  children: [
                    Container(
                      padding: const EdgeInsets.only(bottom: 5),
                      height: MediaQuery.of(context).size.width * 0.3,
                      width: MediaQuery.of(context).size.width * 0.55,
                      child: BlurHash(
                        image: messageUrl,
                        hash: hash!,
                      ),
                    ),
                    Text(
                      message!,
                      style: MyTheme.bodyTextMessage.copyWith(
                          color: isMe! ? Colors.white : Colors.grey[800]),
                    ),
                  ],
                ),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(top: 5),
            child: Row(
              mainAxisAlignment:
                  isMe! ? MainAxisAlignment.end : MainAxisAlignment.start,
              children: [
                if (!isMe!)
                  SizedBox(
                    width: 40,
                  ),
                isMe!
                    ? Icon(
                        Icons.done_all,
                        size: 20,
                        color: !isRead!
                            ? MyTheme.bodyTextTime.color
                            : Palette.mainColor,
                      )
                    : SizedBox(),
                SizedBox(
                  width: 8,
                ),
                Text(
                  readTimestamp(time!),
                  style: MyTheme.bodyTextTime,
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

String readTimestamp(int timestamp) {
  var now = DateTime.now();
  var format = DateFormat('HH:mm a');
  var date = DateTime.fromMillisecondsSinceEpoch(timestamp);
  var diff = now.difference(date);
  var time = '';

  if (diff.inSeconds <= 0 ||
      diff.inSeconds > 0 && diff.inMinutes == 0 ||
      diff.inMinutes > 0 && diff.inHours == 0 ||
      diff.inHours > 0 && diff.inDays == 0) {
    time = format.format(date);
  } else if (diff.inDays > 0 && diff.inDays < 7) {
    if (diff.inDays == 1) {
      time = diff.inDays.toString() + ' DAY AGO';
    } else {
      time = diff.inDays.toString() + ' DAYS AGO';
    }
  } else {
    if (diff.inDays == 7) {
      time = (diff.inDays / 7).floor().toString() + ' WEEK AGO';
    } else {
      time = (diff.inDays / 7).floor().toString() + ' WEEKS AGO';
    }
  }

  return time;
}
