import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:retalk/bloc/add_friends_bloc/add_friends_bloc.dart';
import 'package:retalk/bloc/add_friends_bloc/add_friends_event.dart';
import 'package:retalk/bloc/add_friends_bloc/add_friends_state.dart';
import 'package:retalk/models/friends.dart';
import 'package:retalk/screens/chat_screen/app_theme.dart';
import 'package:retalk/screens/helper/constants.dart';
import 'package:retalk/screens/login_screen/login_pallete.dart';
import 'package:retalk/screens/widgets/loading_shimmer.dart';
import 'package:retalk/screens/widgets/profile_image.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:retalk/service/locator.dart';
import 'package:retalk/service/navigator_service.dart';

class AddFriends extends StatefulWidget {
  const AddFriends({Key? key}) : super(key: key);

  @override
  _AddFriendsState createState() => _AddFriendsState();
}

class _AddFriendsState extends State<AddFriends> {
  TextEditingController searchCo = new TextEditingController();
  List<DocumentSnapshot> search = [];
  Timer? searchOnStoppedTyping;

  @override
  void initState() {
    super.initState();
  }

  bloc(dynamic event) {
    BlocProvider.of<AddFriendsBloc>(context).add(event);
  }

  addFriends(DocumentSnapshot data, int i) {
    FriendsList added = new FriendsList();
    added.aboutMe = data[FireStoreKey.aboutMe];
    added.email = data[FireStoreKey.email];
    added.imageUrl = data[FireStoreKey.image];
    added.userId = data[FireStoreKey.userId];
    added.userName = data[FireStoreKey.userName];
    added.documentId = data[FireStoreKey.documentId];

    bloc(SetupAddFriends(data: added, index: i, context: context));
  }

  @override
  Widget build(BuildContext context) {
    double insets = MediaQuery.of(context).viewInsets.bottom;
    return BlocListener<AddFriendsBloc, AddFriendsState>(
      listener: (context, state) {
        if (state is SearchFriendComplete) {
          setState(() {
            search = state.result;
          });
        }
        if (state is AddFriendComplete) {
          setState(() {
            locator<NavigatorService>().goBack();
          });
        }
      },
      child: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            Container(
              child: (search.length > 0)
                  ? ListView.builder(
                      reverse: true,
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      shrinkWrap: true,
                      physics: ScrollPhysics(),
                      itemCount: search.length,
                      itemBuilder: (context, int index) {
                        return AnimationConfiguration.staggeredList(
                          position: index,
                          duration: const Duration(milliseconds: 375),
                          child: SlideAnimation(
                            verticalOffset: 50.0,
                            child: FadeInAnimation(
                              child:
                                  BlocBuilder<AddFriendsBloc, AddFriendsState>(
                                builder: (context, state) {
                                  if (state is AddFriendsProgress &&
                                      index == state.index) {
                                    return ChatTileShimmer(
                                      aboutMe: search[index]
                                          [FireStoreKey.aboutMe],
                                      profileName: search[index]
                                          [FireStoreKey.userName],
                                      profileUrl: search[index]
                                          [FireStoreKey.image],
                                    );
                                  } else {
                                    return AddFriendsTile(
                                      aboutMe: search[index]
                                          [FireStoreKey.aboutMe],
                                      profileName: search[index]
                                          [FireStoreKey.userName],
                                      profileUrl: search[index]
                                          [FireStoreKey.image],
                                      addTap: () =>
                                          addFriends(search[index], index),
                                    );
                                  }
                                },
                              ),
                            ),
                          ),
                        );
                      },
                    )
                  : Container(
                      padding: const EdgeInsets.all(20),
                      child: Column(
                        children: [
                          Icon(
                            Icons.error_outline,
                            size: 25,
                            color: Palette.mainColor,
                          ),
                          SizedBox(height: 5),
                          Text(
                            'You must know excatly your friend username',
                            style: TextStyle(
                                fontSize: 13,
                                fontWeight: FontWeight.w500,
                                color: Palette.mainColor),
                          ),
                        ],
                      ),
                    ),
            ),
            Container(
              padding: EdgeInsets.only(
                top: 10,
                bottom: insets == 0 ? 20 : insets,
              ),
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 5),
                child: Row(
                  children: [
                    Expanded(
                      child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 5),
                        height: 50,
                        child: TextFormField(
                          controller: searchCo,
                          cursorColor: Palette.mainColor,
                          decoration: addFriendsDecor(),
                        ),
                      ),
                    ),
                    TextButton(
                      onPressed: () => bloc(SearchFriendsEvent(searchCo.text)),
                      style: TextButton.styleFrom(
                        side:
                            BorderSide(width: 1, color: Palette.facebookColor),
                        minimumSize: Size(0, 50),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20)),
                        primary: Colors.white,
                        backgroundColor: Palette.facebookColor,
                      ),
                      child: Padding(
                        padding: const EdgeInsets.only(left: 5.0, right: 5),
                        child: Row(
                          children: [
                            Icon(Icons.search),
                            SizedBox(width: 5),
                            Text('Search')
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class AddFriendsTile extends StatelessWidget {
  final String? profileUrl;
  final String? profileName;
  final String? aboutMe;
  final Function()? addTap;
  final Function()? bodyTap;
  AddFriendsTile({
    this.aboutMe,
    this.addTap,
    this.bodyTap,
    this.profileName,
    this.profileUrl,
  });
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 20),
      child: Row(
        children: [
          CircleWithErrorBuilder(
            size: 28,
            imgProfileUrl: profileUrl!,
          ),
          SizedBox(
            width: 20,
          ),
          GestureDetector(
            onTap: () {},
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  profileName!,
                  style: MyTheme.heading2.copyWith(
                    fontSize: 16,
                  ),
                ),
                Text(
                  aboutMe!,
                  style: MyTheme.bodyText1,
                ),
              ],
            ),
          ),
          Spacer(),
          GestureDetector(
            onTap: addTap,
            child: Container(
              padding: const EdgeInsets.only(left: 15, top: 10, bottom: 10),
              child: Icon(
                Icons.person_add,
                color: Palette.mainColor,
              ),
            ),
          )
        ],
      ),
    );
  }
}
