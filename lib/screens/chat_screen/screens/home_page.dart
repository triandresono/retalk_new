import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:retalk/bloc/chat_page_bloc/chat_page_bloc.dart';
import 'package:retalk/models/users.dart';
import 'package:retalk/screens/helper/constants.dart';
import 'package:retalk/screens/login_screen/login_pallete.dart';
import 'package:retalk/screens/widgets/keep_alive_page.dart';
import 'package:flutter/material.dart';
import 'package:retalk/service/database_methods.dart';
import 'package:retalk/service/locator.dart';
import 'package:retalk/service/navigator_service.dart';
import '../widgets/widgets.dart';
import '../screens/screen.dart';

class HomePage extends StatefulWidget {
  final Users? user;
  HomePage({Key? key, this.user}) : super(key: key);
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with TickerProviderStateMixin {
  TabController? tabController;
  int currentTabIndex = 0;
  Users? user;
  DatabaseMethods db = new DatabaseMethods();

  void onTabChange() {
    setState(() {
      currentTabIndex = tabController!.index;
      print(currentTabIndex);
    });
  }

  @override
  void initState() {
    tabController = TabController(length: 2, vsync: this);

    tabController!.addListener(() {
      onTabChange();
    });
    super.initState();
  }

  @override
  void dispose() {
    tabController!.addListener(() {
      onTabChange();
    });

    tabController!.dispose();

    super.dispose();
  }

  Future<Users?> getUser() async {
    user = await db.getUser();
    return user;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: getUser(),
      builder: (context, snap) {
        if (snap.hasData) {
          return Scaffold(
            backgroundColor: Palette.mainColor,
            body: Column(
              children: [
                SafeArea(
                  child: Padding(
                    padding: EdgeInsets.only(left: 16, right: 16, top: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top: 8.0, bottom: 8),
                          child: Text(
                            "RETALK",
                            style: TextStyle(
                                fontSize: 25,
                                fontWeight: FontWeight.w300,
                                color: Palette.backgroundColor),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            locator<NavigatorService>().navigateToWithArgmnt(
                                PageConstants.USER_PROFILE, user);
                          },
                          child: Container(
                            padding: const EdgeInsets.all(8),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(30),
                            ),
                            child: Icon(
                              Icons.person,
                              color: Palette.backgroundColor,
                              size: 25,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                MyTabBar(tabController: tabController!),
                Expanded(
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 5),
                    decoration: BoxDecoration(
                        color: Palette.backgroundColor,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(25),
                          topRight: Radius.circular(25),
                        )),
                    child: TabBarView(
                      physics: NeverScrollableScrollPhysics(),
                      controller: tabController,
                      children: [
                        KeepAlivePage(
                            child: BlocProvider<ChatPageBloc>(
                          create: (context) => ChatPageBloc(),
                          child: ChatPage(),
                        )),
                        KeepAlivePage(
                          child: Container(),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
            floatingActionButton: FloatingActionButton(
              onPressed: () {
                switch (currentTabIndex) {
                  case 0:
                    return getFriendList();
                  case 1:
                    return;
                }
              },
              backgroundColor: Palette.mainColor,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15),
              ),
              child: Icon(
                currentTabIndex == 0
                    ? Icons.message_outlined
                    : currentTabIndex == 1
                        ? Icons.add_rounded
                        : Icons.call,
                color: Palette.backgroundColor,
              ),
            ),
          );
        } else {
          return Container();
        }
      },
    );
  }

  getFriendList() {
    locator<NavigatorService>().navigateTo(PageConstants.FRIEND_LIST);
  }
}
