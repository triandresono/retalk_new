import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:retalk/bloc/media_message_bloc/media_message_bloc.dart';
import 'package:retalk/bloc/media_message_bloc/media_message_event.dart';
import 'package:retalk/bloc/media_message_bloc/media_message_state.dart';
import 'package:retalk/screens/login_screen/login_pallete.dart';
import 'package:retalk/service/locator.dart';
import 'package:retalk/service/navigator_service.dart';

class MediaMessage extends StatefulWidget {
  final File? image;
  final String? imageUrl;
  final String? userName;
  final double? aspectRatio;

  const MediaMessage({
    Key? key,
    this.image,
    this.imageUrl,
    this.userName,
    this.aspectRatio,
  }) : super(key: key);
  @override
  _MediaMessageState createState() => _MediaMessageState();
}

class _MediaMessageState extends State<MediaMessage> {
  TextEditingController msgCo = new TextEditingController();

  bloc(dynamic event) {
    BlocProvider.of<MediaMessageBloc>(context).add(event);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<MediaMessageBloc, MediaMessageState>(
      listener: (context, state) {
        if (state is AddMessageMediaComplete) {
          locator<NavigatorService>().goBack(state.result);
        }
      },
      child: Scaffold(
        // resizeToAvoidBottomPadding: false,
        backgroundColor: Colors.black87,
        appBar: AppBar(
          elevation: 3,
          backgroundColor: Palette.mainColor,
          titleSpacing: 5,
          automaticallyImplyLeading: false,
          title: Stack(
            children: <Widget>[
              Row(
                children: [
                  GestureDetector(
                    onTap: () => locator<NavigatorService>().goBack(),
                    child: Container(
                      margin: EdgeInsets.only(right: 15),
                      height: 40,
                      width: 40,
                      child: Center(
                        child: Icon(Icons.arrow_back),
                      ),
                    ),
                  ),
                  Row(
                    children: [
                      CircleAvatar(
                        radius: 20,
                        backgroundImage: CachedNetworkImageProvider(
                          widget.imageUrl!,
                        ),
                      ),
                      SizedBox(width: 10),
                      Text(
                        widget.userName!,
                        style: TextStyle(
                          color: Palette.backgroundColor,
                          fontWeight: FontWeight.w300,
                          fontSize: 20,
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ],
          ),
        ),
        body: Center(
          child: Container(
            height: MediaQuery.of(context).size.height,
            child: Image.file(widget.image!),
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: BlocBuilder<MediaMessageBloc, MediaMessageState>(
          builder: (context, state) {
            if (state is AddMessageMediaProgress) {
              return loadingWidget();
            } else {
              return buildMediaTextField2(
                msgCo: msgCo,
                sendTap: () async {
                  FocusScope.of(context).unfocus();
                  bloc(AddMediaMEssage(widget.image!, msgCo.text));
                },
              );
            }
          },
        ),
      ),
    );
  }

  Container loadingWidget() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 30, vertical: 15),
      color: Palette.mainColor.withOpacity(0.5),
      child: Row(
        children: [
          SizedBox(width: 10),
          Expanded(
            child: Container(
              height: 60,
              child: Center(
                child: Row(
                  children: [
                    Center(
                      child: new CircularProgressIndicator(
                        strokeWidth: 3.5,
                        valueColor:
                            AlwaysStoppedAnimation(Palette.backgroundColor),
                      ),
                    ),
                    SizedBox(width: 10),
                    Expanded(
                      child: Text(
                        'Uploading image...',
                        style: TextStyle(color: Palette.backgroundColor),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Container buildMediaTextField2({
    TextEditingController? msgCo,
    Function()? sendTap,
    Function()? choiceTap,
  }) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
      color: Palette.mainColor.withOpacity(0.5),
      // height: 100,
      child: Row(
        children: [
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 14),
              height: 60,
              decoration: BoxDecoration(
                color: Colors.grey[200],
                borderRadius: BorderRadius.circular(30),
              ),
              child: Row(
                children: [
                  SizedBox(
                    width: 10,
                  ),
                  Expanded(
                    child: TextField(
                      controller: msgCo,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: 'Type your message ...',
                        hintStyle: TextStyle(color: Colors.grey[500]),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            width: 16,
          ),
          GestureDetector(
            onTap: sendTap,
            child: CircleAvatar(
              backgroundColor: Palette.mainColor,
              radius: 25,
              child: Icon(
                Icons.send,
                color: Colors.white,
              ),
            ),
          )
        ],
      ),
    );
  }
}

class MediaClass {
  String? imageUrl;
  String? mediaCOntent;
  String? blurHash;

  MediaClass({this.imageUrl, this.mediaCOntent, this.blurHash});
}
