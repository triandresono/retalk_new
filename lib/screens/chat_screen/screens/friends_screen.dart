import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:retalk/bloc/add_friends_bloc/add_friends_bloc.dart';
import 'package:retalk/bloc/friendlist_bloc/friendlist_bloc.dart';
import 'package:retalk/bloc/friendlist_bloc/friendlist_event.dart';
import 'package:retalk/bloc/friendlist_bloc/friendlist_state.dart';
import 'package:retalk/models/friends.dart';
import 'package:retalk/screens/chat_screen/screens/add_friends.dart';
import 'package:retalk/screens/helper/constants.dart';
import 'package:retalk/screens/login_screen/login_pallete.dart';
import 'package:retalk/screens/widgets/loading_shimmer.dart';
import 'package:retalk/screens/widgets/profile_image.dart';
import 'package:retalk/service/locator.dart';
import 'package:retalk/service/navigator_service.dart';

import '../app_theme.dart';

class FriendsScreen extends StatefulWidget {
  final bool? isInView;
  const FriendsScreen({
    Key? key,
    this.isInView,
  }) : super(key: key);

  @override
  _FriendsScreenState createState() => _FriendsScreenState();
}

class _FriendsScreenState extends State<FriendsScreen> {
  bool isSearch = false;
  Stream<QuerySnapshot>? friendList;
  TextEditingController searchCo = new TextEditingController();
  Timer? searchOnStoppedTyping;

  @override
  void initState() {
    bloc(SetFriendList(''));
    super.initState();
  }

  bloc(dynamic event) {
    BlocProvider.of<FriendListBloc>(context).add(event);
  }

  _onChangeHandler(value) {
    const duration = Duration(milliseconds: 800);
    if (searchOnStoppedTyping != null) {
      setState(() => searchOnStoppedTyping!.cancel());
    }
    setState(() => searchOnStoppedTyping =
        new Timer(duration, () => bloc(SetFriendList(value.toLowerCase()))));
  }

  chatFriends(DocumentSnapshot data, int index) {
    List<String> nameSearch = [];
    List<String> emailSearch = [];
    FriendsList added = new FriendsList();
    added.aboutMe = data[FireStoreKey.aboutMe];
    added.email = data[FireStoreKey.email];
    added.imageUrl = data[FireStoreKey.image];
    added.userId = data[FireStoreKey.userId];
    added.userName = data[FireStoreKey.userName];
    added.documentId = data[FireStoreKey.documentId];
    List nameRes = data[FireStoreKey.nameSearch];
    List emailRes = data[FireStoreKey.emailSearch];
    nameRes.forEach((a) {
      nameSearch.add(a);
    });
    emailRes.forEach((b) {
      emailSearch.add(b);
    });
    added.nameSearch = nameSearch;
    added.emailSearch = emailSearch;

    bloc(CreateChatRoom(data: added, index: index));
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<FriendListBloc, FriendListState>(
      listener: (context, state) {
        if (state is CreateChatComplete) {
          locator<NavigatorService>()
              .navigateToWithArgmnt(PageConstants.CHAT_ROOM, state.result);
        }
      },
      child: Scaffold(
        backgroundColor: Palette.backgroundColor,
        appBar: AppBar(
          backgroundColor: Palette.mainColor,
          title: Padding(
            padding: const EdgeInsets.only(top: 15.0, bottom: 15),
            child: AnimatedSwitcher(
              duration: Duration(milliseconds: 350),
              transitionBuilder: (c, a) =>
                  SizeTransition(child: c, sizeFactor: a),
              switchInCurve: Curves.easeIn,
              switchOutCurve: Curves.easeOut,
              child: (!isSearch)
                  ? Container(
                      key: Key('1'),
                      child: Text(
                        "Friends",
                        style: TextStyle(
                            fontSize: 22,
                            fontWeight: FontWeight.w300,
                            color: Palette.backgroundColor),
                      ),
                    )
                  : Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(25.0)),
                        color: Palette.backgroundColor,
                      ),
                      key: Key('2'),
                      child: TextFormField(
                        controller: searchCo,
                        cursorColor: Palette.mainColor,
                        decoration: addFriendsDecor(),
                        onChanged: (val) async {
                          _onChangeHandler(val);
                        },
                      ),
                    ),
            ),
          ),
          actions: [
            GestureDetector(
              onTap: () {
                setState(() {
                  isSearch = !isSearch;
                });
              },
              child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: Icon(Icons.search),
              ),
            ),
          ],
        ),
        extendBody: false,
        extendBodyBehindAppBar: false,
        body: BlocListener<FriendListBloc, FriendListState>(
          listener: (context, state) {
            if (state is GetFriendsComplete) {
              setState(() {
                friendList = state.result;
              });
            }
          },
          child: SingleChildScrollView(
            child: Column(
              children: [
                StreamBuilder<QuerySnapshot>(
                  stream: friendList,
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return Column(
                        children: [
                          (snapshot.data!.docs.length > 0)
                              ? Column(
                                  children: [
                                    AddFriendsTile(onTap: () => addFriends()),
                                    ListView.builder(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 20),
                                        shrinkWrap: true,
                                        physics: ScrollPhysics(),
                                        itemCount: snapshot.data!.docs.length,
                                        itemBuilder: (context, index) {
                                          DocumentSnapshot query =
                                              snapshot.data!.docs[index];

                                          return AnimationConfiguration
                                              .staggeredList(
                                            position: index,
                                            duration: const Duration(
                                                milliseconds: 375),
                                            child: SlideAnimation(
                                              verticalOffset: 50.0,
                                              child: FadeInAnimation(child:
                                                  BlocBuilder<FriendListBloc,
                                                      FriendListState>(
                                                builder: (context, state) {
                                                  if (state
                                                          is CreateChatProgress &&
                                                      index == state.index) {
                                                    return ChatTileShimmer(
                                                      aboutMe: query[
                                                          FireStoreKey.aboutMe],
                                                      profileName: query[
                                                          FireStoreKey
                                                              .userName],
                                                      profileUrl: query[
                                                          FireStoreKey.image],
                                                    );
                                                  } else {
                                                    return GestureDetector(
                                                      onTap: () => chatFriends(
                                                          query, index),
                                                      child: FriendListTile(
                                                        aboutMe: query[
                                                            FireStoreKey
                                                                .aboutMe],
                                                        profileName: query[
                                                            FireStoreKey
                                                                .userName],
                                                        profileUrl: query[
                                                            FireStoreKey.image],
                                                        addTap: () =>
                                                            chatFriends(
                                                                query, index),
                                                      ),
                                                    );
                                                  }
                                                },
                                              )),
                                            ),
                                          );
                                        }),
                                  ],
                                )
                              : Center(
                                  child: Padding(
                                    padding: const EdgeInsets.only(top: 250.0),
                                    child: GestureDetector(
                                      onTap: () => addFriends(),
                                      child: Container(
                                        decoration: BoxDecoration(
                                            color: Palette.mainColor,
                                            borderRadius: BorderRadius.only(
                                              topLeft: Radius.circular(25),
                                              topRight: Radius.circular(25),
                                              bottomLeft: Radius.circular(0),
                                              bottomRight: Radius.circular(12),
                                            )),
                                        child: Padding(
                                          padding: const EdgeInsets.all(15.0),
                                          child: Column(
                                            mainAxisSize: MainAxisSize.min,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              Icon(
                                                Icons.add,
                                                color: Palette.backgroundColor,
                                              ),
                                              SizedBox(height: 10),
                                              Text(
                                                'Tap here to add friends',
                                                style: TextStyle(
                                                  color:
                                                      Palette.backgroundColor,
                                                  fontWeight: FontWeight.w300,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                )
                        ],
                      );
                    } else {
                      return Padding(
                        padding: const EdgeInsets.only(top: 200.0),
                        child: Center(
                          child: CircularProgressIndicator(
                            strokeWidth: 3.5,
                            valueColor: AlwaysStoppedAnimation(
                              Palette.mainColor,
                            ),
                          ),
                        ),
                      );
                    }
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  addFriends() {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        backgroundColor: Palette.backgroundColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(20)),
        ),
        builder: (context) {
          return BlocProvider(
              create: (context) {
                return AddFriendsBloc();
              },
              child: AddFriends());
        });
  }
}

class FriendListTile extends StatelessWidget {
  final String? profileUrl;
  final String? profileName;
  final String? aboutMe;
  final Function()? addTap;
  final Function()? bodyTap;
  FriendListTile({
    Key? key,
    this.aboutMe,
    this.addTap,
    this.bodyTap,
    this.profileName,
    this.profileUrl,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 20),
      color: Palette.backgroundColor,
      child: Row(
        children: [
          CircleWithErrorBuilder(
            size: 28,
            imgProfileUrl: profileUrl!,
          ),
          SizedBox(
            width: 20,
          ),
          GestureDetector(
            onTap: () {},
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  profileName!,
                  style: MyTheme.heading2.copyWith(
                    fontSize: 16,
                  ),
                ),
                Text(
                  aboutMe!,
                  style: MyTheme.bodyText1,
                ),
              ],
            ),
          ),
          Spacer(),
          GestureDetector(
            onTap: addTap,
            child: Container(
              padding: const EdgeInsets.only(left: 15, top: 10, bottom: 10),
              child: Icon(
                Icons.message_outlined,
                color: Palette.mainColor,
              ),
            ),
          )
        ],
      ),
    );
  }
}

class AddFriendsTile extends StatelessWidget {
  final Function()? onTap;
  AddFriendsTile({this.onTap});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        margin: const EdgeInsets.only(top: 20),
        padding: const EdgeInsets.symmetric(horizontal: 20),
        color: Palette.backgroundColor,
        child: Row(
          children: [
            CircleAvatar(
              backgroundColor: Palette.mainColor,
              radius: 28,
              child: Icon(
                Icons.person_add,
                color: Palette.backgroundColor,
                size: 30,
              ),
            ),
            SizedBox(
              width: 5,
            ),
            // Spacer(),
            Container(
              padding: const EdgeInsets.only(left: 15, top: 10, bottom: 10),
              child: Text(
                'Add new friends',
                style: MyTheme.heading2.copyWith(
                  fontSize: 16,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
