class ChatModel {
  String? message;
  String? imagesContent;
  String? type;
  String? sendBy;
  String? hash;
  bool? isRead;
  int? time;

  ChatModel({this.message, this.sendBy, this.time, this.type});

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["message"] = message;
    map["type"] = type;
    map["sendBy"] = sendBy;
    map["time"] = time;
    map["isRead"] = isRead;
    map["hash"] = hash;
    map["imagesContent"] = imagesContent;
    return map;
  }
}
