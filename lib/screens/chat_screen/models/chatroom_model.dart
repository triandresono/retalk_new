class ChatRoomModel {
  List<String?>? users;
  String? chatRoomId;

  ChatRoomModel({this.chatRoomId, this.users});

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["chatRoomId"] = chatRoomId;
    map["users"] = users;
    return map;
  }
}
