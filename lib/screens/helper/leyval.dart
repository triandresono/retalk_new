class KeyVal {
  String? label;
  String? value;
  KeyVal({this.label, this.value});
}

class GenderKeyVal {
  static List<KeyVal> gender = [
    KeyVal(label: 'Male', value: 'Male'),
    KeyVal(label: 'Female', value: 'Female'),
    KeyVal(label: 'Prefer not to say', value: 'Prefer not to say')
  ];
}
