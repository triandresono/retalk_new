class Constants {
  static const String type_normal = 'normal';
  static const String type_image = 'image';

  //default
  static const String default_about_me = 'Hey there! lets Retalk';
  static const String default_fb = 'defaultfblogin';
  static const String default_google_login = 'defaultgooglelogin';
  static const String default_gender = 'Prefer not to say';
  static const String empty_image_url =
      'https://firebasestorage.googleapis.com/v0/b/com-id-retalk.appspot.com/o/no_image.png?alt=media&token=43d2ea29-0d1d-4b1a-89cb-50189861ca4e';
  static const String default_background =
      'https://firebasestorage.googleapis.com/v0/b/com-id-retalk.appspot.com/o/retalk_default_wallpaper.jpg?alt=media&token=4a978fb2-4e6f-4924-aa46-c77bb3a607fe';
}

class PageConstants {
  static const String LOGIN_MAIN = '/loginmain';
  static const String HOME_PAGE = '/homepage';
  static const String USER_PROFILE = '/userprofile';
  static const String FRIEND_LIST = '/friendlist';
  static const String CHAT_ROOM = '/chatroom';
}

class FireStoreKey {
  //Store Collection
  static const String USERS = 'Users';
  static const String FRIENDS = 'Friends';
  static const String CHATROOM = 'ChatRoom';
  static const String INROOMSTATS = 'InRoomStatus';
  static const String CHATS = 'Chats';
  static const String FRIENDLISTS = 'FriendList';
  static const String CHATPEERS = 'ChatPeers';
  //Param Key
  //Users
  static const String userName = 'userName';
  static const String email = 'email';
  static const String userId = 'userId';
  static const String aboutMe = 'aboutMe';
  static const String image = 'imageUrl';
  static const String documentId = 'documentId';
  static const String nameSearch = 'nameSearch';
  static const String emailSearch = 'emailSearch';
  static const String friendList = 'friendList';
  static const String blockList = 'blockList';
  static const String chatRoomIDs = 'chatRoomIDs';
  //Chats
  static const String isRead = 'isRead';
  static const String sendBy = 'sendBy';
  static const String imagesContent = 'imagesContent';
  static const String type = 'type';
  static const String message = 'message';
  static const String chatTime = 'time';
  static const String hash = 'hash';
  //ChatRoom
  static const String isInRoom = 'isInRoom';
  static const String users = 'users';
  static const String lastChat = 'lastChat';
  static const String lastChatType = 'lastChatType';
  static const String chatRoomId = 'chatRoomId';
  //ChatPeers
  static const String peers = 'Peers';
}

class DatabaseKey {
  static const String userId = 'userId';
  static const String imageUrl = 'imageUrl';
  static const String aboutMe = 'aboutMe';
  static const String email = 'email';
  static const String userName = 'userName';
  static const String friendList = 'friendList';
  static const String blockList = 'blockList';
  static const String chatRoomIDs = 'chatRoomIDs';
  static const String documentId = 'documentId';
  static const String isLoggedIn = 'isLoggedIn';
  static const String chatRooms = 'chatRooms';
}
