List<String> generateStringKey(String? data) {
  List<String> caseSearchList = [''];
  String temp = "";
  for (int i = 0; i < data!.length; i++) {
    temp = (temp + data[i]).toLowerCase();
    caseSearchList.add(temp);
  }
  return caseSearchList;
}
