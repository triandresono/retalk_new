import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:retalk/models/users.dart';
import 'package:retalk/screens/login_screen/login_main.dart';
import 'package:retalk/screens/login_screen/login_pallete.dart';
import 'package:retalk/screens/widgets/profile_image.dart';
import 'package:retalk/service/database_methods.dart';
import 'package:retalk/service/firebase_auth.dart';

class ProfileScreen extends StatefulWidget {
  final Users? user;
  ProfileScreen({Key? key, this.user}) : super(key: key);
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  List<String> listChoice = ['Logout'];
  AuthMethods auth = new AuthMethods();
  DatabaseMethods db = new DatabaseMethods();
  bool editAboutMe = false;
  bool editGender = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Palette.backgroundColor,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            alignment: Alignment.center,
            overflow: Overflow.visible,
            children: [
              AspectRatio(
                aspectRatio: 4 / 3,
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(25),
                    bottomRight: Radius.circular(25),
                  )),
                  child: Container(
                    color: Palette.mainColor,
                  ),
                ),
              ),
              Positioned(
                bottom: -65.0,
                child: Stack(
                  children: [
                    Container(
                      child: CircleWithErrorBuilder(
                        imgProfileUrl: widget.user!.imageUrl!,
                        size: 80,
                      ),
                    ),
                    Positioned(
                      bottom: 0,
                      left: 110,
                      child: GestureDetector(
                        behavior: HitTestBehavior.opaque,
                        onTap: () {},
                        child: Container(
                          padding: EdgeInsets.all(3),
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Palette.mainColor,
                          ),
                          child: Icon(
                            Icons.image,
                            color: Palette.backgroundColor,
                            size: 30,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Positioned(
                top: 10,
                right: 10,
                child: SafeArea(
                  child: Padding(
                    padding: EdgeInsets.only(left: 16, right: 16, top: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        GestureDetector(
                          onTap: () {},
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(30),
                              color: Palette.mainColor,
                            ),
                            child: PopupMenuButton<String>(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(10.0),
                                ),
                              ),
                              icon: Icon(
                                Icons.person,
                                color: Palette.backgroundColor,
                              ),
                              onSelected: (a) async {
                                await db.dbClear();
                                await auth.signOut();
                                Navigator.pushReplacement(
                                    context,
                                    CupertinoPageRoute(
                                      builder: (context) => LoginMain(),
                                    ));
                              },
                              itemBuilder: (BuildContext context) {
                                return listChoice.map((String choice) {
                                  return PopupMenuItem<String>(
                                      height: 30,
                                      value: choice,
                                      child: popUpMenuItem(choice));
                                }).toList();
                              },
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.only(top: 70, left: 25),
            height: 50,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  child: Text(
                    widget.user!.userName!,
                    style: TextStyle(
                        fontSize: 25,
                        fontWeight: FontWeight.w400,
                        color: Palette.mainColor),
                  ),
                ),
                SizedBox(width: 10),
                GestureDetector(
                  behavior: HitTestBehavior.opaque,
                  onTap: () {},
                  child: Container(
                    padding: EdgeInsets.all(3),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Palette.backgroundColor,
                    ),
                    child: Icon(
                      Icons.edit,
                      color: Palette.mainColor,
                      size: 27,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 16, right: 16, top: 25),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ListTile(
                  leading: Icon(
                    Icons.mail_outline,
                    color: Palette.mainColor,
                    size: 23,
                  ),
                  trailing: Icon(
                    Icons.edit,
                    color: Palette.mainColor,
                    size: 23,
                  ),
                  title: Transform.translate(
                    offset: Offset(-16, 0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          child: Text(
                            'Email',
                            style: profileLabelStyle(),
                          ),
                        ),
                        Container(
                          child: Text(
                            widget.user!.email!,
                            style: profileTextStyle(),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                ListTile(
                  leading: Icon(
                    Icons.message,
                    color: Palette.mainColor,
                    size: 23,
                  ),
                  trailing: Icon(
                    Icons.edit,
                    color: Palette.mainColor,
                    size: 23,
                  ),
                  title: Transform.translate(
                    offset: Offset(-16, 0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          child: Text(
                            'About me',
                            style: profileLabelStyle(),
                          ),
                        ),
                        Container(
                          child: Text(
                            widget.user!.aboutMe!,
                            style: profileTextStyle(),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget popUpMenuItem(String choice) {
    return Row(
      children: [
        Icon(
          Icons.logout,
          color: Palette.mainColor,
        ),
        SizedBox(width: 7),
        Text(
          choice,
          style: TextStyle(color: Palette.mainColor),
        ),
      ],
    );
  }
}

TextStyle profileTextStyle() {
  return TextStyle(
      fontSize: 16, fontWeight: FontWeight.w200, color: Palette.mainColor);
}

TextStyle profileLabelStyle() {
  return TextStyle(
      fontSize: 18, fontWeight: FontWeight.w400, color: Palette.mainColor);
}
