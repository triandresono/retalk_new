import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:retalk/screens/helper/constants.dart';

class CircleWithErrorBuilder extends StatelessWidget {
  CircleWithErrorBuilder({
    this.size,
    this.imgProfileUrl,
    this.onTap,
    this.errorWidget,
  });
  final String? imgProfileUrl;
  final double? size;
  final Function()? onTap;
  final Widget? errorWidget;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: onTap,
        child: CachedNetworkImage(
          imageUrl: imgProfileUrl!,
          imageBuilder: (context, imageProvider) => new CircleAvatar(
              radius: size,
              backgroundImage: imageProvider,
              backgroundColor: Colors.white),
          errorWidget: (context, url, error) =>
              errorWidget ??
              CircleAvatar(
                radius: size,
                backgroundColor: Colors.white,
                backgroundImage: NetworkImage(Constants.empty_image_url),
              ),
        ));
  }
}
