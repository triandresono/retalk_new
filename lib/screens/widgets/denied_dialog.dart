import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:retalk/screens/login_screen/login_pallete.dart';
import 'package:retalk/service/locator.dart';
import 'package:retalk/service/navigator_service.dart';

class DeniedDialog extends StatelessWidget {
  final String? subHeading;
  DeniedDialog({this.subHeading});
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      backgroundColor: Palette.backgroundColor,
      title: Text(
        'Permission needed',
        style: TextStyle(
          fontSize: 16,
          fontWeight: FontWeight.w600,
        ),
      ),
      content: Text(
        subHeading!,
        style: TextStyle(
          color: Palette.mainColor,
        ),
      ),
      actions: <Widget>[
        FlatButton(
          onPressed: () {
            locator<NavigatorService>().goBack();
          },
          child: Text(
            'Cancel',
            style: TextStyle(color: Colors.red),
          ),
        ),
        FlatButton(
          onPressed: () {
            openAppSettings();
          },
          child: Text('Open Settings'),
        ),
      ],
    );
  }
}

showDeniedDialog(String text, BuildContext context) {
  showDialog(
      context: context,
      builder: (context) {
        return DeniedDialog(subHeading: text);
      });
}
