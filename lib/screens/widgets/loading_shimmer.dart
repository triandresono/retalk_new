import 'package:flutter/material.dart';
import 'package:retalk/screens/chat_screen/app_theme.dart';
import 'package:retalk/screens/login_screen/login_pallete.dart';
import 'package:retalk/screens/widgets/profile_image.dart';
import 'package:shimmer/shimmer.dart';

class ChatTileShimmer extends StatelessWidget {
  final String? profileUrl;
  final String? profileName;
  final String? aboutMe;
  final Function()? addTap;
  final Function()? bodyTap;
  ChatTileShimmer({
    this.aboutMe,
    this.addTap,
    this.bodyTap,
    this.profileName,
    this.profileUrl,
  });
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 20),
      child: Shimmer.fromColors(
        period: Duration(milliseconds: 1000),
        baseColor: Colors.grey[300]!,
        highlightColor: Colors.grey[100]!,
        child: Row(
          children: [
            CircleWithErrorBuilder(
              size: 28,
              imgProfileUrl: profileUrl!,
            ),
            SizedBox(
              width: 20,
            ),
            GestureDetector(
              onTap: () {},
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    profileName!,
                    style: MyTheme.heading2.copyWith(
                      fontSize: 16,
                    ),
                  ),
                  Text(
                    aboutMe!,
                    style: MyTheme.bodyText1,
                  ),
                ],
              ),
            ),
            Spacer(),
            GestureDetector(
              onTap: addTap,
              child: Container(
                padding: const EdgeInsets.only(left: 15, top: 10, bottom: 10),
                child: Icon(
                  Icons.person_add,
                  color: Palette.mainColor,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
