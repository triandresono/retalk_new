import 'package:flutter/material.dart';
import 'package:retalk/screens/login_screen/login_pallete.dart';

Widget loginButton({Function()? onTap, double? width, String? text}) {
  return GestureDetector(
    onTap: onTap,
    child: Container(
      padding: const EdgeInsets.only(top: 12, bottom: 12),
      width: width,
      decoration: BoxDecoration(
        color: Palette.mainColor,
        borderRadius: BorderRadius.circular(5),
      ),
      child: Text(
        text ?? 'LOGIN',
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: 15,
          color: Palette.backgroundColor,
          fontWeight: FontWeight.w600,
        ),
      ),
    ),
  );
}
