import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:retalk/bloc/sign_in_bloc/sign_in_bloc.dart';
import 'package:retalk/bloc/sign_up_bloc/sign_up_bloc.dart';
import 'package:retalk/screens/login_screen/login_pallete.dart';
import 'package:retalk/screens/login_screen/sign_in.dart';
import 'package:retalk/screens/login_screen/sign_up.dart';
import 'package:retalk/screens/widgets/keep_alive_page.dart';

class LoginMain extends StatefulWidget {
  @override
  _LoginMainState createState() => _LoginMainState();
}

class _LoginMainState extends State<LoginMain> with TickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  TabController? tabController;
  int selectedIndex = 1;

  @override
  void initState() {
    tabController = new TabController(length: 2, vsync: this, initialIndex: 1);
    tabController!.addListener((() {
      if (tabController!.indexIsChanging) {
        setState(() {
          selectedIndex = tabController!.index;
        });
      }
    }));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // double screenHeight = MediaQuery.of(context).size.height;
    double screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      key: _scaffoldKey,
      appBar: null,
      backgroundColor: Palette.mainColor,
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: screenWidth,
              color: Palette.mainColor,
              padding: const EdgeInsets.only(bottom: 100.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 100),
                  Padding(
                    padding: const EdgeInsets.only(left: 20.0),
                    child: RichText(
                      text: selectedIndex == 0 ? signInSPan() : signUpSpan(),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 35.0),
                    child: Text(
                      selectedIndex == 0
                          ? "Sign in to Continue"
                          : "Sign up to Continue",
                      style: TextStyle(
                        letterSpacing: 1,
                        color: Colors.white,
                      ),
                    ),
                  )
                ],
              ),
            ),
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                  color: Palette.backgroundColor,
                  borderRadius: BorderRadius.vertical(top: Radius.circular(25)),
                ),
                width: screenWidth,
                child: Column(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        borderRadius:
                            BorderRadius.vertical(top: Radius.circular(25)),
                      ),
                      padding: const EdgeInsets.all(8.0),
                      child: TabBar(
                        controller: tabController,
                        indicatorColor: Colors.transparent,
                        unselectedLabelColor: Palette.mainColor,
                        indicator: BoxDecoration(
                          color: Palette.mainColor,
                          borderRadius: BorderRadius.circular(25),
                        ),
                        labelColor: Palette.backgroundColor,
                        onTap: (index) {
                          setState(() {
                            selectedIndex = index;
                          });
                        },
                        tabs: [
                          Tab(
                            child: Center(
                              child: Text('Sign in'),
                            ),
                          ),
                          Tab(
                            child: Center(
                              child: Text('Sign up'),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      child: Container(
                        child: TabBarView(
                          controller: tabController,
                          physics: NeverScrollableScrollPhysics(),
                          children: [
                            KeepAlivePage(
                                child: BlocProvider(
                              create: (context) {
                                return SignInBloc();
                              },
                              child: SignIn(
                                mainController: tabController,
                                scaffoldKey: _scaffoldKey,
                              ),
                            )),
                            KeepAlivePage(
                                child: BlocProvider(
                              create: (context) {
                                return SignUpBloc();
                              },
                              child: SignUp(
                                mainController: tabController,
                                scaffoldKey: _scaffoldKey,
                              ),
                            )),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
