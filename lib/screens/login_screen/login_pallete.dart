import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

class Palette {
  static const Color iconColor = Color(0xFFB6C7D1);
  static const Color activeColor = Color(0xFF09126C);
  static const Color textColor1 = Color(0XFFA7BCC7);
  static const Color textColor2 = Color(0XFF9BB3C0);
  static const Color facebookColor = Color(0xFF3B5999);
  static const Color googleColor = Color(0xFFDE4B39);
  static const Color backgroundColor = Color(0xFFECF3F9);
  static const Color mainColor = Color(0xFF286090);
}

TextSpan signInSPan() {
  return TextSpan(
    text: "Welcome",
    style: TextStyle(
      fontSize: 25,
      letterSpacing: 2,
      color: Palette.backgroundColor,
    ),
    children: [
      TextSpan(
        text: ' Back',
        style: TextStyle(
          fontSize: 25,
          fontWeight: FontWeight.bold,
          color: Palette.backgroundColor,
          fontStyle: FontStyle.italic,
        ),
      )
    ],
  );
}

TextSpan signUpSpan() {
  return TextSpan(
    text: "Welcome to",
    style: TextStyle(
      fontSize: 25,
      letterSpacing: 2,
      color: Palette.backgroundColor,
    ),
    children: [
      TextSpan(
        text: ' Retalk',
        style: TextStyle(
          fontSize: 25,
          fontWeight: FontWeight.bold,
          color: Palette.backgroundColor,
          fontStyle: FontStyle.italic,
        ),
      )
    ],
  );
}

InputDecoration emailDecor() {
  return InputDecoration(
    prefixIcon: Icon(
      Icons.mail_outline,
      color: Palette.mainColor,
    ),
    enabledBorder: OutlineInputBorder(
      borderSide: BorderSide(color: Palette.mainColor),
      borderRadius: BorderRadius.all(Radius.circular(25.0)),
    ),
    focusedBorder: OutlineInputBorder(
      borderSide: BorderSide(color: Palette.mainColor),
      borderRadius: BorderRadius.all(Radius.circular(25.0)),
    ),
    errorBorder: OutlineInputBorder(
      borderSide: BorderSide(color: Palette.mainColor),
      borderRadius: BorderRadius.all(Radius.circular(25.0)),
    ),
    focusedErrorBorder: OutlineInputBorder(
      borderSide: BorderSide(color: Palette.mainColor),
      borderRadius: BorderRadius.all(Radius.circular(25.0)),
    ),
    contentPadding: EdgeInsets.all(10),
    hintText: 'Email',
    hintStyle: TextStyle(
      fontSize: 14,
      color: Palette.mainColor.withOpacity(0.5),
    ),
  );
}

InputDecoration userNameDecor() {
  return InputDecoration(
    prefixIcon: Icon(
      Icons.people,
      color: Palette.mainColor,
    ),
    enabledBorder: OutlineInputBorder(
      borderSide: BorderSide(color: Palette.mainColor),
      borderRadius: BorderRadius.all(Radius.circular(25.0)),
    ),
    focusedBorder: OutlineInputBorder(
      borderSide: BorderSide(color: Palette.mainColor),
      borderRadius: BorderRadius.all(Radius.circular(25.0)),
    ),
    errorBorder: OutlineInputBorder(
      borderSide: BorderSide(color: Palette.mainColor),
      borderRadius: BorderRadius.all(Radius.circular(25.0)),
    ),
    focusedErrorBorder: OutlineInputBorder(
      borderSide: BorderSide(color: Palette.mainColor),
      borderRadius: BorderRadius.all(Radius.circular(25.0)),
    ),
    contentPadding: EdgeInsets.all(10),
    hintText: 'Username',
    hintStyle: TextStyle(
      fontSize: 14,
      color: Palette.mainColor.withOpacity(0.5),
    ),
  );
}

InputDecoration passDecor({bool? hide, Function()? visible, String? hint}) {
  return InputDecoration(
    prefixIcon: Icon(
      Icons.lock_outline,
      color: Palette.mainColor,
    ),
    suffixIcon: GestureDetector(
      onTap: visible,
      child: Container(
        padding: const EdgeInsets.all(5),
        child: Icon(
          (hide!) ? Icons.visibility : Icons.visibility_off,
          color: Palette.mainColor,
        ),
      ),
    ),
    floatingLabelBehavior: FloatingLabelBehavior.always,
    enabledBorder: OutlineInputBorder(
      borderSide: BorderSide(color: Palette.mainColor),
      borderRadius: BorderRadius.all(Radius.circular(25.0)),
    ),
    focusedBorder: OutlineInputBorder(
      borderSide: BorderSide(color: Palette.mainColor),
      borderRadius: BorderRadius.all(Radius.circular(25.0)),
    ),
    errorBorder: OutlineInputBorder(
      borderSide: BorderSide(color: Palette.mainColor),
      borderRadius: BorderRadius.all(Radius.circular(25.0)),
    ),
    focusedErrorBorder: OutlineInputBorder(
      borderSide: BorderSide(color: Palette.mainColor),
      borderRadius: BorderRadius.all(Radius.circular(25.0)),
    ),
    contentPadding: EdgeInsets.all(10),
    hintText: hint ?? 'Password',
    hintStyle: TextStyle(
      fontSize: 14,
      color: Palette.mainColor.withOpacity(0.5),
    ),
  );
}

InputDecoration searchDecor() {
  return InputDecoration(
    prefixIcon: Icon(
      Icons.search,
      color: Palette.mainColor,
    ),
    enabledBorder: OutlineInputBorder(
      borderSide: BorderSide(color: Palette.mainColor),
      borderRadius: BorderRadius.all(Radius.circular(25.0)),
    ),
    focusedBorder: OutlineInputBorder(
      borderSide: BorderSide(color: Palette.mainColor),
      borderRadius: BorderRadius.all(Radius.circular(25.0)),
    ),
    errorBorder: OutlineInputBorder(
      borderSide: BorderSide(color: Palette.mainColor),
      borderRadius: BorderRadius.all(Radius.circular(25.0)),
    ),
    focusedErrorBorder: OutlineInputBorder(
      borderSide: BorderSide(color: Palette.mainColor),
      borderRadius: BorderRadius.all(Radius.circular(25.0)),
    ),
    contentPadding: EdgeInsets.all(10),
    hintText: 'Search...',
    hintStyle: TextStyle(
      fontSize: 14,
      color: Palette.mainColor.withOpacity(0.5),
    ),
  );
}

InputDecoration addFriendsDecor() {
  return InputDecoration(
    enabledBorder: OutlineInputBorder(
      borderSide: BorderSide(color: Palette.mainColor),
      borderRadius: BorderRadius.all(Radius.circular(25.0)),
    ),
    focusedBorder: OutlineInputBorder(
      borderSide: BorderSide(color: Palette.mainColor),
      borderRadius: BorderRadius.all(Radius.circular(25.0)),
    ),
    errorBorder: OutlineInputBorder(
      borderSide: BorderSide(color: Palette.mainColor),
      borderRadius: BorderRadius.all(Radius.circular(25.0)),
    ),
    focusedErrorBorder: OutlineInputBorder(
      borderSide: BorderSide(color: Palette.mainColor),
      borderRadius: BorderRadius.all(Radius.circular(25.0)),
    ),
    contentPadding: EdgeInsets.all(10),
    hintText: 'Search...',
    hintStyle: TextStyle(
      fontSize: 14,
      color: Palette.mainColor.withOpacity(0.5),
    ),
  );
}

TextStyle loginTextStyle() {
  return TextStyle(
    fontSize: 15,
    color: Palette.mainColor,
    fontWeight: FontWeight.w600,
  );
}

TextStyle liteTextStyle() {
  return TextStyle(
    fontSize: 14,
    color: Palette.mainColor,
  );
}
