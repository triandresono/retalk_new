import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:retalk/bloc/sign_up_bloc/sign_up_bloc.dart';
import 'package:retalk/bloc/sign_up_bloc/sign_up_event.dart';
import 'package:retalk/bloc/sign_up_bloc/sign_up_state.dart';
import 'package:retalk/models/users.dart';
import 'package:retalk/screens/login_screen/login_pallete.dart';

enum Gender { Male, Female, Privacy }

class SignUp extends StatefulWidget {
  final TabController? mainController;
  final GlobalKey<ScaffoldState>? scaffoldKey;
  SignUp({Key? key, this.mainController, this.scaffoldKey}) : super(key: key);
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  final formKey = GlobalKey<FormState>();
  Gender selectedGender = Gender.Male;
  TextEditingController emailCo = new TextEditingController();
  TextEditingController passCo = new TextEditingController();
  TextEditingController userNameCo = new TextEditingController();
  TextEditingController pass2ndCo = new TextEditingController();
  TextEditingController genderCo = new TextEditingController();
  bool hide = true;
  bool confirmPass = true;

  bloc(dynamic event) {
    BlocProvider.of<SignUpBloc>(context).add(event);
  }

  submit() {
    if (formKey.currentState!.validate()) {
      Users users = new Users();
      users.userName = userNameCo.text;
      users.email = emailCo.text;
      users.password = passCo.text;
      bloc(SetupSignUpEvent(user: users));
    }
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    return BlocListener<SignUpBloc, SignUpState>(
      listener: (context, state) {
        if (state is SignUpSuccess) {
          emailCo.clear();
          userNameCo.clear();
          pass2ndCo.clear();
          passCo.clear();
          genderCo.clear();
          widget.scaffoldKey!.currentState!.showSnackBar(new SnackBar(
              backgroundColor: Palette.mainColor,
              elevation: 3,
              content: Row(
                children: [
                  Icon(Icons.check, color: Palette.backgroundColor),
                  SizedBox(width: 5),
                  Text('Your account has been created successfully'),
                ],
              )));
        }
        if (state is UsernameExist) {
          widget.scaffoldKey!.currentState!.showSnackBar(new SnackBar(
              backgroundColor: Colors.redAccent,
              elevation: 3,
              content: Row(
                children: [
                  Icon(Icons.close, color: Palette.backgroundColor),
                  SizedBox(width: 5),
                  Text('Your username already been used'),
                ],
              )));
        }
        if (state is EmailExist) {
          widget.scaffoldKey!.currentState!.showSnackBar(new SnackBar(
              backgroundColor: Colors.redAccent,
              elevation: 3,
              content: Row(
                children: [
                  Icon(Icons.close, color: Palette.backgroundColor),
                  SizedBox(width: 5),
                  Text('Your email already been used'),
                ],
              )));
        }
      },
      child: Container(
        child: Form(
          key: formKey,
          child: Container(
            padding: const EdgeInsets.only(left: 36, right: 36, top: 24),
            child: SingleChildScrollView(
              controller: ScrollController(),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  TextFormField(
                    controller: emailCo,
                    cursorColor: Palette.mainColor,
                    decoration: emailDecor(),
                    validator: (val) => emailValidate(val),
                  ),
                  SizedBox(height: 20),
                  TextFormField(
                    controller: userNameCo,
                    cursorColor: Palette.mainColor,
                    decoration: userNameDecor(),
                    validator: (val) => nickValidate(val),
                  ),
                  SizedBox(height: 20),
                  TextFormField(
                    controller: passCo,
                    obscureText: hide,
                    cursorColor: Palette.mainColor,
                    decoration: passDecor(
                        hide: hide,
                        visible: () {
                          setState(() {
                            hide = !hide;
                          });
                        }),
                    validator: (val) => passValidate(val),
                  ),
                  SizedBox(height: 20),
                  TextFormField(
                    controller: pass2ndCo,
                    obscureText: confirmPass,
                    cursorColor: Palette.mainColor,
                    decoration: passDecor(
                        hint: 'Confirm password',
                        hide: confirmPass,
                        visible: () {
                          setState(() {
                            confirmPass = !confirmPass;
                          });
                        }),
                    validator: (val) => secondPassValidate(val),
                  ),
                  SizedBox(height: 35),
                  buildButton(
                    onTap: () => submit(),
                    width: screenWidth,
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 30),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'Already have an Account ?',
                          style: loginTextStyle(),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        GestureDetector(
                          onTap: () {
                            widget.mainController!.animateTo(0);
                          },
                          child: Text(
                            'Sign IN',
                            style: TextStyle(
                              fontSize: 15,
                              color: Palette.mainColor,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 30),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget buildButton({Function()? onTap, double? width}) {
    return BlocBuilder<SignUpBloc, SignUpState>(
      builder: (context, state) {
        if (state is SignUpOnProgress) {
          return Center(
            child: Container(
              padding: const EdgeInsets.all(4),
              width: width! * 0.2,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Palette.mainColor,
              ),
              child: Center(
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(Palette.backgroundColor),
                ),
              ),
            ),
          );
        } else {
          return GestureDetector(
            onTap: onTap,
            child: Container(
              padding: const EdgeInsets.only(top: 12, bottom: 12),
              width: width,
              decoration: BoxDecoration(
                color: Palette.mainColor,
                borderRadius: BorderRadius.circular(25),
              ),
              child: Text(
                'SIGN UP',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 15,
                  color: Palette.backgroundColor,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
          );
        }
      },
    );
  }

  //validator
  String? emailValidate(String? val) {
    RegExp regex = new RegExp(
        r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+");

    if (val!.isEmpty) {
      return 'Email cant be empty';
    } else if (!regex.hasMatch(val)) {
      return 'Enter correct email';
    } else {
      return null;
    }
  }

  String? nickValidate(String? val) {
    if (val!.isEmpty) {
      return 'Username cant be empty';
    } else {
      return null;
    }
  }

  String? passValidate(String? val) {
    if (val!.isEmpty) {
      return 'Password cant be empty';
    } else if (val.length < 6) {
      return "Enter Password 6+ characters";
    } else {
      return null;
    }
  }

  String? secondPassValidate(String? val) {
    if (val!.isEmpty) {
      return 'Confirmation password cant be empty';
    } else if (passCo.text != pass2ndCo.text) {
      return "Wrong confirmation password";
    } else {
      return null;
    }
  }

  String? getGender(Gender selected) {
    String result = '';
    if (selected == Gender.Male) {
      result = 'Male';
    } else if (selected == Gender.Female) {
      result = 'Female';
    } else {
      result = 'Prefer not to say';
    }
    return result;
  }
}
