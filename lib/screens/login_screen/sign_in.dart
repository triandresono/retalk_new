import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:retalk/bloc/sign_in_bloc/sign_in_bloc.dart';
import 'package:retalk/bloc/sign_in_bloc/sign_in_event.dart';
import 'package:retalk/bloc/sign_in_bloc/sign_in_state.dart';
import 'package:retalk/models/users.dart';
import 'package:retalk/screens/helper/constants.dart';
import 'package:retalk/screens/login_screen/login_pallete.dart';
import 'package:retalk/service/locator.dart';
import 'package:retalk/service/navigator_service.dart';

class SignIn extends StatefulWidget {
  final TabController? mainController;
  final GlobalKey<ScaffoldState>? scaffoldKey;
  SignIn({
    Key? key,
    this.mainController,
    this.scaffoldKey,
  }) : super(key: key);
  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  bool wrongData = false;
  TextEditingController emailCo = new TextEditingController();
  TextEditingController passCo = new TextEditingController();
  final formKey = new GlobalKey<FormState>();
  bool hide = true;

  bloc(dynamic event) {
    BlocProvider.of<SignInBloc>(context).add(event);
  }

  login() {
    if (formKey.currentState!.validate()) {
      FocusScope.of(context).unfocus();
      Users user = new Users();
      user.email = emailCo.text.trim();
      user.password = passCo.text.trim();
      bloc(SetupSignIn(user: user));
    }
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    return BlocListener<SignInBloc, SignInState>(
      listener: (context, state) {
        if (state is FacebookLoginComplete) {
          locator<NavigatorService>()
              .navigateToWithArgmnt(PageConstants.HOME_PAGE, state.user);
        }
        if (state is GoogleLoginComplete) {
          locator<NavigatorService>()
              .navigateToWithArgmnt(PageConstants.HOME_PAGE, state.user);
        }
        if (state is SignInComplete) {
          locator<NavigatorService>()
              .navigateToWithArgmnt(PageConstants.HOME_PAGE, state.user);
        }
        if (state is SignInFailed) {
          String a = 'Wrong password, please check again';
          String b = 'User doesn\'t exist, please check your email';
          widget.scaffoldKey!.currentState!.showSnackBar(new SnackBar(
              backgroundColor: Colors.redAccent,
              elevation: 3,
              content: Row(
                children: [
                  Icon(Icons.close, color: Palette.backgroundColor),
                  SizedBox(width: 5),
                  Text(state.message == 'ERROR_WRONG_PASSWORD' ? a : b),
                ],
              )));
        }
      },
      child: Container(
        child: Form(
          key: formKey,
          child: Container(
            padding: const EdgeInsets.only(left: 36, right: 36, top: 24),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  TextFormField(
                    controller: emailCo,
                    cursorColor: Palette.mainColor,
                    decoration: emailDecor(),
                    validator: (val) => emailValidate(val),
                  ),
                  SizedBox(height: 20),
                  TextFormField(
                    controller: passCo,
                    obscureText: hide,
                    cursorColor: Palette.mainColor,
                    decoration: passDecor(
                        hide: hide,
                        visible: () {
                          setState(() {
                            hide = !hide;
                          });
                        }),
                    validator: (val) => passValidate(val),
                  ),
                  SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text(
                        'Forgot Password?',
                        style: loginTextStyle(),
                      ),
                    ],
                  ),
                  SizedBox(height: 20),
                  buildButton(
                    onTap: () => login(),
                    width: screenWidth,
                  ),
                  Stack(
                    children: [
                      Column(
                        children: [
                          SizedBox(
                            height: 20,
                          ),
                          Text(
                            "Or Sign in with",
                            style: loginTextStyle(),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 15),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                buttonFacebook(),
                                SizedBox(
                                  width: 8,
                                ),
                                buttonGoogle(),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Container(
                            margin: const EdgeInsets.only(top: 15, bottom: 40),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  'Don\'t have an Account ?',
                                  style: loginTextStyle(),
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                GestureDetector(
                                  onTap: () {
                                    widget.mainController!.animateTo(1);
                                  },
                                  child: Text(
                                    'Sign UP',
                                    style: TextStyle(
                                      fontSize: 15,
                                      color: Palette.mainColor,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget buildButton({Function()? onTap, double? width}) {
    return BlocBuilder<SignInBloc, SignInState>(
      builder: (context, state) {
        if (state is SignInOnProgress) {
          return Center(
            child: Container(
              padding: const EdgeInsets.all(4),
              width: width! * 0.2,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Palette.mainColor,
              ),
              child: Center(
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(Palette.backgroundColor),
                ),
              ),
            ),
          );
        } else {
          return GestureDetector(
            onTap: onTap,
            child: Container(
              padding: const EdgeInsets.only(top: 12, bottom: 12),
              width: width,
              decoration: BoxDecoration(
                color: Palette.mainColor,
                borderRadius: BorderRadius.circular(25),
              ),
              child: Text(
                'LOGIN',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 15,
                  color: Palette.backgroundColor,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
          );
        }
      },
    );
  }

  Widget buttonFacebook() {
    return BlocBuilder<SignInBloc, SignInState>(
      builder: (context, state) {
        if (state is FacebookLoginProgress) {
          return TextButton(
            onPressed: () {},
            style: TextButton.styleFrom(
              side: BorderSide(width: 1, color: Palette.facebookColor),
              minimumSize: Size(145, 30),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30)),
              primary: Colors.white,
              backgroundColor: Palette.facebookColor,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Center(
                  child: Container(
                    height: 25,
                    width: 25,
                    child: CircularProgressIndicator(
                      strokeWidth: 2.0,
                      valueColor:
                          AlwaysStoppedAnimation(Palette.backgroundColor),
                    ),
                  ),
                ),
              ],
            ),
          );
        } else {
          return TextButton(
            onPressed: () => bloc(SetupFacebookSignIn()),
            style: TextButton.styleFrom(
              side: BorderSide(width: 1, color: Palette.facebookColor),
              minimumSize: Size(145, 40),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              primary: Colors.white,
              backgroundColor: Palette.facebookColor,
            ),
            child: Row(
              children: [
                // Icon(MaterialCommunityIcons.facebook),
                SizedBox(width: 5),
                Text('Facebook')
              ],
            ),
          );
        }
      },
    );
  }

  Widget buttonGoogle() {
    return BlocBuilder<SignInBloc, SignInState>(
      builder: (context, state) {
        if (state is GoogleLoginProgress) {
          return TextButton(
            onPressed: () {},
            style: TextButton.styleFrom(
              side: BorderSide(width: 1, color: Palette.googleColor),
              minimumSize: Size(145, 30),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30)),
              primary: Colors.white,
              backgroundColor: Palette.googleColor,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Center(
                  child: Container(
                    height: 25,
                    width: 25,
                    child: CircularProgressIndicator(
                      strokeWidth: 2.0,
                      valueColor:
                          AlwaysStoppedAnimation(Palette.backgroundColor),
                    ),
                  ),
                ),
              ],
            ),
          );
        } else {
          return TextButton(
            onPressed: () => bloc(SetupGoogleSignIn()),
            style: TextButton.styleFrom(
              side: BorderSide(width: 1, color: Palette.googleColor),
              minimumSize: Size(145, 40),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20)),
              primary: Colors.white,
              backgroundColor: Palette.googleColor,
            ),
            child: Row(
              children: [
                // Icon(MaterialCommunityIcons.google),
                SizedBox(width: 5),
                Text('Google')
              ],
            ),
          );
        }
      },
    );
  }

  String? emailValidate(String? val) {
    RegExp regex = new RegExp(
        r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+");

    if (val!.isEmpty) {
      return 'Email cant be empty';
    } else if (!regex.hasMatch(val)) {
      return 'Enter correct email';
    } else {
      return null;
    }
  }

  String? passValidate(String? val) {
    if (val!.isEmpty) {
      return 'Password cant be empty';
    } else if (val.length < 6) {
      return "Enter Password 6+ characters";
    } else {
      return null;
    }
  }
}
