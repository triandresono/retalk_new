import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:retalk/bloc/splash_bloc/splash_bloc.dart';
import 'package:retalk/bloc/splash_bloc/splash_event.dart';
import 'package:retalk/bloc/splash_bloc/splash_state.dart';
import 'package:retalk/screens/helper/constants.dart';
import 'package:retalk/screens/login_screen/login_pallete.dart';
import 'package:retalk/service/locator.dart';
import 'package:retalk/service/navigator_service.dart';

class HomeSplash extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: null,
      body: BlocProvider(
          create: (context) {
            return SplashBloc();
          },
          child: SplashScreen()),
    );
  }
}

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((_) async {
      await Future.delayed(Duration(seconds: 1));
      bloc(InitialSetup(context));
    });
  }

  bloc(dynamic event) {
    BlocProvider.of<SplashBloc>(context).add(event);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<SplashBloc, SplashState>(
      listener: (context, state) {
        if (state is InitialSetupComplete && state.result) {
          locator<NavigatorService>()
              .navigateReplaceTo(PageConstants.HOME_PAGE);
        }
        if (state is InitialSetupComplete && !state.result) {
          locator<NavigatorService>()
              .navigateReplaceTo(PageConstants.LOGIN_MAIN);
        }
      },
      child: Container(
        color: Palette.backgroundColor,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: 200.0,
                child: Stack(
                  children: <Widget>[
                    Center(
                      child: Container(
                        width: 200,
                        height: 200,
                        child: new CircularProgressIndicator(
                          strokeWidth: 3.5,
                          valueColor: AlwaysStoppedAnimation(Palette.mainColor),
                        ),
                      ),
                    ),
                    Center(child: Image.asset('assets/logo/retalk_icon.png')),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
