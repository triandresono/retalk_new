import 'package:retalk/models/users.dart';

class SignUpEvent {}

class SetupSignUpEvent extends SignUpEvent {
  final Users user;
  SetupSignUpEvent({required this.user});
}
