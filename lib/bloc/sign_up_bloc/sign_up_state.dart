class SignUpState {}

class InitialSignUp extends SignUpState {}

class SignUpOnProgress extends SignUpState {}

class SignUpSuccess extends SignUpState {}

class UsernameExist extends SignUpState {}

class EmailExist extends SignUpState {}

class SignUpFailed extends SignUpState {
  final String message;
  SignUpFailed(this.message);
}
