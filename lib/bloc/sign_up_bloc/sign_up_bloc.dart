import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:retalk/bloc/sign_up_bloc/sign_up_event.dart';
import 'package:retalk/bloc/sign_up_bloc/sign_up_state.dart';
import 'package:retalk/models/users.dart';
import 'package:retalk/screens/helper/constants.dart';
import 'package:retalk/service/database_methods.dart';
import 'package:retalk/service/firebase_auth.dart';
import 'package:retalk/service/firebase_instance.dart';

class SignUpBloc extends Bloc<SignUpEvent, SignUpState> {
  CloudFirestoreInstance fire = new CloudFirestoreInstance();
  DatabaseMethods db = new DatabaseMethods();
  AuthMethods auth = new AuthMethods();

  SignUpBloc() : super(InitialSignUp());

  @override
  Stream<SignUpState> mapEventToState(SignUpEvent event) async* {
    try {
      if (event is SetupSignUpEvent) {
        yield SignUpOnProgress();

        bool emailIsExist = await fire.checkMail(event.user.email);
        bool nickIsExist = await fire.checkUsername(event.user.userName);

        if (emailIsExist) {
          yield EmailExist();
        } else if (nickIsExist) {
          yield UsernameExist();
        } else {
          Users users = event.user;
          String id = await auth.signUpWithEmail(users.email, users.password);
          users.aboutMe = Constants.default_about_me;
          users.imageUrl = Constants.empty_image_url;
          users.userId = id;
          users.friendList = [];
          users.blockList = [];
          users.chatRoomIDs = [];

          await fire.uploadUserInfo(users.toMap(), users.userId);
          yield SignUpSuccess();
        }
      }
    } catch (e) {
      yield SignUpFailed(e.toString());
    }
  }
}
