import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:retalk/bloc/splash_bloc/splash_event.dart';
import 'package:retalk/bloc/splash_bloc/splash_state.dart';
import 'package:retalk/screens/widgets/denied_dialog.dart';
import 'package:retalk/service/database_methods.dart';

class SplashBloc extends Bloc<SplashEvent, SplashState> {
  SplashBloc() : super(SplashInitial());

  @override
  Stream<SplashState> mapEventToState(SplashEvent event) async* {
    try {
      if (event is InitialSetup) {
        await requestPermission(event.context);
        bool? check = await getLoggedInState();
        await Future.delayed(Duration(seconds: 3));
        if (check == null) {
          yield InitialSetupComplete(false);
        } else {
          yield InitialSetupComplete(check);
        }
      }
    } catch (e) {
      yield InitialSetupFailed(message: e.toString());
    }
  }

  Future requestPermission(BuildContext context) async {
    PermissionStatus? galleryStatus;
    PermissionStatus? cameraStatus;
    Permission? permissionGallery = Permission.storage;
    Permission? permissionCamera = Permission.camera;

    await permissionGallery.request().then((res) => galleryStatus = res);
    await permissionCamera.request().then((res) => cameraStatus = res);

    if (galleryStatus == PermissionStatus.permanentlyDenied) {
      showDeniedDialog('Storage permission is need to select photos', context);
    } else if (cameraStatus == PermissionStatus.permanentlyDenied) {
      showDeniedDialog('Camera permission is need take a photo', context);
    } else if (galleryStatus == PermissionStatus.granted) {
      return;
    } else if (cameraStatus == PermissionStatus.granted) {
      return;
    }
  }

  Future<bool?> getLoggedInState() async {
    bool? isLoggedIn = false;
    await DatabaseMethods.getUserLoggedInSharedPreference().then((val) {
      isLoggedIn = val;
    });
    return isLoggedIn!;
  }
}
