import 'package:flutter/cupertino.dart';

class SplashEvent {}

class InitialSetup extends SplashEvent {
  final BuildContext context;
  InitialSetup(this.context);
}
