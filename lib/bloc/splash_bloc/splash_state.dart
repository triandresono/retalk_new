class SplashState {}

class SplashInitial extends SplashState {}

class InitialSetupComplete extends SplashState {
  final bool result;
  InitialSetupComplete(this.result);
}

class InitialSetupFailed extends SplashState {
  final String message;
  InitialSetupFailed({required this.message});
}
