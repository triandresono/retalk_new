import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:retalk/models/friends.dart';

class ChatPageState {}

class InitialChatPage extends ChatPageState {}

class GetChatPageProgress extends ChatPageState {}

class ChatIsEmpty extends ChatPageState {}

class GetChatPageComplete extends ChatPageState {
  final Stream<QuerySnapshot> stream;

  GetChatPageComplete(this.stream);
}

class GoChatProgress extends ChatPageState {}

class GoChatComplete extends ChatPageState {
  final FriendsList result;

  GoChatComplete(this.result);
}

class ChatPageFailed extends ChatPageState {
  final String message;

  ChatPageFailed(this.message);
}
