import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:retalk/bloc/chat_page_bloc/chat_page_event.dart';
import 'package:retalk/bloc/chat_page_bloc/chat_page_state.dart';
import 'package:retalk/models/friends.dart';
import 'package:retalk/models/users.dart';
import 'package:retalk/service/database_methods.dart';
import 'package:retalk/service/firebase_instance.dart';

class ChatPageBloc extends Bloc<ChatPageEvent, ChatPageState> {
  ChatPageBloc() : super(InitialChatPage());

  Users? dbUser;
  Stream<QuerySnapshot>? result;
  DatabaseMethods db = new DatabaseMethods();
  CloudFirestoreInstance store = new CloudFirestoreInstance();

  @override
  Stream<ChatPageState> mapEventToState(ChatPageEvent event) async* {
    try {
      dbUser = await db.getUser();
      if (event is GetChatPage) {
        yield GetChatPageProgress();
        result = await store.getChatPeers(dbUser!, event.val);
        yield GetChatPageComplete(result!);
      } else if (event is GotoChatPage) {
        yield GoChatProgress();
        FriendsList peer = event.peers;
        await store.updateInRoom(event.roomId, dbUser!.userId!, true);
        yield GoChatComplete(peer);
      }
    } catch (e) {
      ChatPageFailed(e.toString());
    }
  }
}
