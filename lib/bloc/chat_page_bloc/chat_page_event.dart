import 'package:retalk/models/friends.dart';

class ChatPageEvent {}

class GetChatPage extends ChatPageEvent {
  final String val;

  GetChatPage(this.val);
}

class GotoChatPage extends ChatPageEvent {
  final String roomId;
  final FriendsList peers;

  GotoChatPage(this.roomId, this.peers);
}
