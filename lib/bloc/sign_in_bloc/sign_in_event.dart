import 'package:retalk/models/users.dart';

class SignInEvent {}

class SetupSignIn extends SignInEvent {
  final Users user;
  SetupSignIn({required this.user});
}

class SetupFacebookSignIn extends SignInEvent {}

class SetupGoogleSignIn extends SignInEvent {}
