import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:retalk/bloc/sign_in_bloc/sign_in_event.dart';
import 'package:retalk/bloc/sign_in_bloc/sign_in_state.dart';
import 'package:retalk/models/users.dart';
import 'package:retalk/screens/helper/constants.dart';
import 'package:retalk/service/database_methods.dart';
import 'package:retalk/service/facebook_login.dart';
import 'package:retalk/service/firebase_auth.dart';
import 'package:retalk/service/firebase_instance.dart';
import 'package:retalk/service/google_login.dart';

class SignInBloc extends Bloc<SignInEvent, SignInState> {
  CloudFirestoreInstance fire = new CloudFirestoreInstance();
  DatabaseMethods db = new DatabaseMethods();
  AuthMethods auth = new AuthMethods();

  SignInBloc() : super(InitialSignInState());

  @override
  Stream<SignInState> mapEventToState(SignInEvent event) async* {
    try {
      if (event is SetupSignIn) {
        yield SignInOnProgress();
        Users user = event.user;
        String? id = await auth.signInWithEmail(user.email, user.password);
        if (id != null) {
          Users result = await fire.getUsersById(id);
          await db.saveUser(user: result);
          await db.notifyLoggedin(isLoggedIn: true);
          yield SignInComplete(result);
        } else {
          yield SignInWrongMailorPass();
        }
      } else if (event is SetupFacebookSignIn) {
        yield FacebookLoginProgress();
        Users user = await FacebookLogin.fbLogin();
        Users result = await signInMethod(user, user.password);
        yield FacebookLoginComplete(result);
      } else if (event is SetupGoogleSignIn) {
        yield GoogleLoginProgress();
        Users user = await GoogleLogin.googleLogin();
        Users result = await signInMethod(user, user.password);
        yield GoogleLoginComplete(result);
      }
    } catch (e) {
      yield SignInFailed(e.toString());
    }
  }

  Future<Users> signInMethod(Users user, String? pass) async {
    Users result = new Users();
    bool emailisExist = await fire.checkMail(user.email);
    if (emailisExist) {
      String id = await auth.signInWithEmail(user.email, pass);
      Users currentUser = await fire.getUsersById(id);
      await db.saveUser(user: currentUser);
      await db.notifyLoggedin(isLoggedIn: true);
      result = currentUser;
    } else {
      Users register = new Users();
      String id = await auth.signUpWithEmail(user.email, pass);
      register.userId = id;
      register.friendList = [];
      register.blockList = [];
      register.chatRoomIDs = [];
      register.email = user.email;
      register.userName = user.userName;
      register.imageUrl = user.imageUrl;
      register.aboutMe = Constants.default_about_me;
      String docId =
          await fire.uploadUserInfo(register.toMap(), register.userId);
      register.documentId = docId;
      await db.saveUser(user: register);
      await db.notifyLoggedin(isLoggedIn: true);
      result = register;
    }
    return result;
  }
}
