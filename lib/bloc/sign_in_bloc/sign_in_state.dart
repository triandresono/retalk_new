import 'package:retalk/models/users.dart';

class SignInState {}

class InitialSignInState extends SignInState {}

class SignInOnProgress extends SignInState {}

class FacebookLoginProgress extends SignInState {}

class GoogleLoginProgress extends SignInState {}

class SignInComplete extends SignInState {
  final Users user;
  SignInComplete(this.user);
}

class FacebookLoginComplete extends SignInState {
  final Users user;
  FacebookLoginComplete(this.user);
}

class GoogleLoginComplete extends SignInState {
  final Users user;
  GoogleLoginComplete(this.user);
}

class SignInWrongMailorPass extends SignInState {}

class SignInFailed extends SignInState {
  final String message;
  SignInFailed(this.message);
}
