import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:retalk/bloc/chat_room_bloc/chat_room_event.dart';
import 'package:retalk/bloc/chat_room_bloc/chat_room_state.dart';
import 'package:retalk/models/users.dart';
import 'package:retalk/service/database_methods.dart';
import 'package:retalk/service/firebase_instance.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:path/path.dart';

class ChatRoomBloc extends Bloc<ChatRoomEvent, ChatRoomState> {
  ChatRoomBloc() : super(InitialChatRoom());

  DatabaseMethods db = new DatabaseMethods();
  CloudFirestoreInstance fire = new CloudFirestoreInstance();
  Users? dbUser;
  Stream<QuerySnapshot>? chatStream;
  Stream<DocumentSnapshot>? onlineStats;

  @override
  Stream<ChatRoomState> mapEventToState(ChatRoomEvent event) async* {
    try {
      dbUser = await db.getUser();
      if (event is GetChatEvent) {
        chatStream = await fire.getConversationMessage(event.roomId);
        await fire.updateIsRead(event.roomId, event.friendName);
        yield GetChatComplete(result: chatStream!, user: dbUser!);
      } else if (event is AddChatEvent) {
        await fire.updateChatPeers(
          dbUser!,
          event.friend,
          event.model.message,
          event.model.type,
          event.model.imagesContent,
        );
        fire.addConversationMessage(
          event.chatRoomId,
          event.model.toMap(),
        );

        yield AddChatComplete();
      } else if (event is GetOnlineStatus) {
        onlineStats = fire.getInRoomStatus(event.roomId, event.friendId);
        yield GetOnlineComplete(result: onlineStats!);
      }
    } catch (e) {
      yield ChatFailed(message: e.toString());
    }
  }

  Future uploadImageToFirebase(File image) async {
    String downloadUrl = '';
    String fileName = basename(image.path);
    Reference firebaseStorageRef =
        FirebaseStorage.instance.ref().child('uploads/$fileName');
    UploadTask uploadTask = firebaseStorageRef.putFile(image);
    TaskSnapshot taskSnapshot = await uploadTask.whenComplete(() => null);
    taskSnapshot.ref.getDownloadURL().then((value) {
      downloadUrl = value;
    });

    return downloadUrl;
  }
}
