import 'package:retalk/models/friends.dart';
import 'package:retalk/screens/chat_screen/models/chat_model.dart';

class ChatRoomEvent {}

class GetChatEvent extends ChatRoomEvent {
  final String roomId;
  final String friendName;
  GetChatEvent({
    required this.roomId,
    required this.friendName,
  });
}

class AddChatEvent extends ChatRoomEvent {
  final ChatModel model;
  final String chatRoomId;
  final FriendsList friend;
  AddChatEvent({
    required this.model,
    required this.chatRoomId,
    required this.friend,
  });
}

class GetOnlineStatus extends ChatRoomEvent {
  final String roomId;
  final String friendId;
  GetOnlineStatus({
    required this.roomId,
    required this.friendId,
  });
}
