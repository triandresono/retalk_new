import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:retalk/models/users.dart';

class ChatRoomState {}

class InitialChatRoom extends ChatRoomState {}

class GetChatComplete extends ChatRoomState {
  Stream<QuerySnapshot> result;
  Users user;
  GetChatComplete({required this.result, required this.user});
}

class GetOnlineComplete extends ChatRoomState {
  Stream<DocumentSnapshot> result;
  GetOnlineComplete({required this.result});
}

class AddChatComplete extends ChatRoomState {}

class ChatFailed extends ChatRoomState {
  final String message;
  ChatFailed({required this.message});
}
