import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:retalk/bloc/friendlist_bloc/friendlist_event.dart';
import 'package:retalk/bloc/friendlist_bloc/friendlist_state.dart';
import 'package:retalk/models/chat_peers.dart';
import 'package:retalk/models/friends.dart';
import 'package:retalk/models/users.dart';
import 'package:retalk/screens/chat_screen/models/chatroom_model.dart';
import 'package:retalk/screens/helper/helper_methods.dart';
import 'package:retalk/service/database_methods.dart';
import 'package:retalk/service/firebase_instance.dart';

class FriendListBloc extends Bloc<FriendListEvent, FriendListState> {
  DatabaseMethods db = new DatabaseMethods();
  CloudFirestoreInstance fire = new CloudFirestoreInstance();
  Users? dbuser;
  Stream<QuerySnapshot>? result;

  FriendListBloc() : super(InitialFriendList());

  @override
  Stream<FriendListState> mapEventToState(FriendListEvent event) async* {
    try {
      dbuser = await db.getUser();
      if (event is SetFriendList) {
        result = await fire.getFriendList(dbuser!.userId!, event.searchVal);
        yield GetFriendsComplete(result!);
      } else if (event is CreateChatRoom) {
        yield CreateChatProgress(index: event.index);
        ChatRoomModel room = new ChatRoomModel();
        FriendsList frend = event.data;
        List<String?> peer = [frend.userName, dbuser!.userName];
        String roomId = fire.getChatRoomId(frend.userName, dbuser!.userName);
        frend.chatRoomId = roomId;
        room.chatRoomId = roomId;
        room.users = peer;
        if (dbuser!.chatRoomIDs!.contains(roomId)) {
          await fire.updateInRoom(event.data.chatRoomId, dbuser!.userId, true);
          yield CreateChatComplete(result: frend);
        } else {
          await fire.updateChatRoomList(
            roomId,
            dbuser!.documentId,
            frend.documentId,
          );
          await fire.createChatRoom(roomId, room.toMap());
          await fire.createInRoomStatus(roomId, dbuser!.userId, frend.userId);
          await createChatPeers(frend, dbuser!);
          await db.updateChatRoomList(roomId);
          yield CreateChatComplete(result: frend);
        }
      }
    } catch (e) {
      yield GetFriendsFailed(message: e.toString());
    }
  }

  createChatPeers(FriendsList friends, Users user) async {
    ChatPeers users = new ChatPeers();
    ChatPeers friend = new ChatPeers();
    List<String> nameSearch = generateStringKey(user.userName);
    List<String> emailSearch = generateStringKey(user.email);

    users.chatRoomId = friends.chatRoomId;
    users.userId = user.userId;
    users.aboutMe = user.aboutMe;
    users.email = user.email;
    users.imageUrl = user.imageUrl;
    users.userName = user.userName;
    users.nameSearch = nameSearch;
    users.emailSearch = emailSearch;
    users.documentId = user.documentId;
    users.imagesContent = '';
    users.lastChatType = '';
    users.lastChat = '';

    friend.chatRoomId = friends.chatRoomId;
    friend.userId = friends.userId;
    friend.aboutMe = friends.aboutMe;
    friend.email = friends.email;
    friend.imageUrl = friends.imageUrl;
    friend.userName = friends.userName;
    friend.nameSearch = friends.nameSearch;
    friend.emailSearch = friends.emailSearch;
    friend.documentId = friends.documentId;
    friend.imagesContent = '';
    friend.lastChatType = '';
    friend.lastChat = '';

    await fire.createChatPeers(friends.chatRoomId, users, friend);
  }
}
