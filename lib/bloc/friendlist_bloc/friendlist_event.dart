import 'package:retalk/models/friends.dart';

class FriendListEvent {}

class SetFriendList extends FriendListEvent {
  final String searchVal;
  SetFriendList(this.searchVal);
}

class SetDeleteFriends extends FriendListEvent {}

class GotoFriendProfile extends FriendListEvent {}

class CreateChatRoom extends FriendListEvent {
  FriendsList data;
  int index;
  CreateChatRoom({required this.data, required this.index});
}
