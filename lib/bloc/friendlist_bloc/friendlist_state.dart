import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:retalk/models/friends.dart';

class FriendListState {}

class InitialFriendList extends FriendListState {}

class GetFriendsProgress extends FriendListState {}

class GetFriendsComplete extends FriendListState {
  Stream<QuerySnapshot> result;
  GetFriendsComplete(this.result);
}

class DeleteFriendsProgress extends FriendListState {}

class DeleteFriendsComplete extends FriendListState {}

class FriendsProfileComplete extends FriendListState {}

class CreateChatProgress extends FriendListState {
  final int index;
  CreateChatProgress({required this.index});
}

class CreateChatComplete extends FriendListState {
  final FriendsList result;
  CreateChatComplete({required this.result});
}

class GetFriendsFailed extends FriendListState {
  final String message;
  GetFriendsFailed({required this.message});
}
