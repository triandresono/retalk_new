import 'dart:io';

class MediaMessageEvent {}

class AddMediaMEssage extends MediaMessageEvent {
  final File image;
  final String mediaContent;

  AddMediaMEssage(this.image, this.mediaContent);
}
