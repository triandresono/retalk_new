import 'dart:io';
import 'dart:typed_data';
import 'package:path/path.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:retalk/bloc/media_message_bloc/media_message_event.dart';
import 'package:retalk/bloc/media_message_bloc/media_message_state.dart';
import 'package:retalk/screens/chat_screen/screens/media_message.dart';
import 'package:blurhash/blurhash.dart';

class MediaMessageBloc extends Bloc<MediaMessageEvent, MediaMessageState> {
  MediaMessageBloc() : super(InitialMediaMessage());

  @override
  Stream<MediaMessageState> mapEventToState(MediaMessageEvent event) async* {
    try {
      if (event is AddMediaMEssage) {
        yield AddMessageMediaProgress();
        String url = await uploadImageToFirebase(event.image);
        String hash = await generateImageHash(event.image);
        MediaClass media = new MediaClass();
        media.imageUrl = url;
        media.mediaCOntent = event.mediaContent;
        media.blurHash = hash;
        yield AddMessageMediaComplete(media);
      }
    } catch (e) {}
  }

  Future<String> uploadImageToFirebase(File file) async {
    String fileName = basename(file.path);
    Reference firebaseStorageRef =
        FirebaseStorage.instance.ref().child('uploads/$fileName');
    UploadTask uploadTask = firebaseStorageRef.putFile(file);
    TaskSnapshot taskSnapshot = await uploadTask.whenComplete(() => null);
    return taskSnapshot.ref.getDownloadURL().then(
      (value) {
        return value;
      },
    );
  }

  Future<String> generateImageHash(File file) async {
    Uint8List bytes = await file.readAsBytes();
    String blurHash = await BlurHash.encode(bytes, 2, 2);
    return blurHash;
  }
}
