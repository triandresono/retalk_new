import 'package:retalk/screens/chat_screen/screens/media_message.dart';

class MediaMessageState {}

class InitialMediaMessage extends MediaMessageState {}

class AddMessageMediaProgress extends MediaMessageState {}

class AddMessageMediaComplete extends MediaMessageState {
  final MediaClass result;

  AddMessageMediaComplete(this.result);
}
