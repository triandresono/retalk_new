import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:retalk/bloc/add_friends_bloc/add_friends_event.dart';
import 'package:retalk/bloc/add_friends_bloc/add_friends_state.dart';
import 'package:retalk/models/friends.dart';
import 'package:retalk/models/users.dart';
import 'package:retalk/screens/helper/helper_methods.dart';
import 'package:retalk/service/database_methods.dart';
import 'package:retalk/service/firebase_instance.dart';

class AddFriendsBloc extends Bloc<AddFriendsEvent, AddFriendsState> {
  AddFriendsBloc() : super(InitialAddFriends());

  DatabaseMethods db = DatabaseMethods();
  CloudFirestoreInstance fire = CloudFirestoreInstance();
  Users? getUser;

  @override
  Stream<AddFriendsState> mapEventToState(AddFriendsEvent event) async* {
    try {
      getUser = await db.getUser();
      if (event is SearchFriendsEvent) {
        yield SearchFriendProgress();
        List<DocumentSnapshot> result = [];
        List<String> friendLists = await db.getFriends();
        result = await fire.friendSearchList(
          event.query,
          getUser!.userName!,
          friendLists,
        );
        yield SearchFriendComplete(result);
      } else if (event is SetupAddFriends) {
        yield AddFriendsProgress(event.index);
        await addFriend(event.data, getUser!);
        yield AddFriendComplete(event.index);
      }
    } catch (e) {
      yield GoneWrong(e.toString());
    }
  }

  Future addFriend(FriendsList friends, Users user) async {
    FriendsList ur = new FriendsList();
    FriendsList fr = friends;
    List<String> usernameSearch = generateStringKey(user.userName);
    List<String> usermailSearch = generateStringKey(user.email);
    ur.userId = user.userId;
    ur.userName = user.userName;
    ur.email = user.email;
    ur.aboutMe = user.aboutMe;
    ur.imageUrl = user.imageUrl;
    ur.documentId = user.documentId;
    ur.nameSearch = usernameSearch;
    ur.emailSearch = usermailSearch;

    List<String> friendnameSearch = generateStringKey(friends.userName);
    List<String> friendMailSearch = generateStringKey(friends.email);
    fr.nameSearch = friendnameSearch;
    fr.emailSearch = friendMailSearch;

    await fire.addFriends(getUser!, fr, ur);
    await db.updateFriend(friends.userId);
  }
}
