import 'package:cloud_firestore/cloud_firestore.dart';

class AddFriendsState {}

class InitialAddFriends extends AddFriendsState {}

class SearchFriendProgress extends AddFriendsState {}

class SearchFriendComplete extends AddFriendsState {
  List<DocumentSnapshot> result;
  SearchFriendComplete(this.result);
}

class AddFriendsProgress extends AddFriendsState {
  final int index;
  AddFriendsProgress(this.index);
}

class AddFriendComplete extends AddFriendsState {
  int index;
  AddFriendComplete(this.index);
}

class GoneWrong extends AddFriendsState {
  String message;
  GoneWrong(this.message);
}
