import 'package:flutter/cupertino.dart';
import 'package:retalk/models/friends.dart';

class AddFriendsEvent {}

class SearchFriendsEvent extends AddFriendsEvent {
  final String query;
  SearchFriendsEvent(this.query);
}

class SetupAddFriends extends AddFriendsEvent {
  final FriendsList data;
  final int index;
  final BuildContext context;
  SetupAddFriends({
    required this.data,
    required this.index,
    required this.context,
  });
}
