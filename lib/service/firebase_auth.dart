import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'facebook_login.dart';
import 'google_login.dart';

class AuthMethods {
  FirebaseAuth _auth = FirebaseAuth.instance;
  String? _userFirebase(User user) {
    return user.uid;
  }

  Future signInWithEmail(String? email, String? password) async {
    try {
      UserCredential result = await _auth.signInWithEmailAndPassword(
        email: email!,
        password: password!,
      );
      User firebaseUser = result.user!;

      return _userFirebase(firebaseUser);
    } catch (e) {
      throw e.toString();
    }
  }

  Future signUpWithEmail(String? email, String? password) async {
    try {
      UserCredential result = await _auth.createUserWithEmailAndPassword(
        email: email!,
        password: password!,
      );

      User firebaseUser = result.user!;

      return _userFirebase(firebaseUser);
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  Future resetPass(String email) async {
    try {
      return await _auth.sendPasswordResetEmail(email: email);
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  Future signOut() async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      await _auth.signOut();
      await GoogleLogin.googleLogout();
      await FacebookLogin.fbLogOut();
      prefs.clear();
    } catch (e) {
      debugPrint(e.toString());
    }
  }
}
