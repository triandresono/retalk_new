import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:retalk/models/chat_peers.dart';
import 'package:retalk/models/friends.dart';
import 'package:retalk/models/users.dart';
import 'package:retalk/screens/helper/constants.dart';

class CloudFirestoreInstance {
  Future<String> uploadUserInfo(userMap, String? userId) async {
    return FirebaseFirestore.instance
        .collection(FireStoreKey.USERS)
        .add(userMap)
        .then((res) {
      FirebaseFirestore.instance
          .collection(FireStoreKey.USERS)
          .doc(res.id)
          .update({FireStoreKey.documentId: res.id});
      return res.id;
    });
  }

  Future<bool> checkUsername(String? username) async {
    QuerySnapshot snapshot = await FirebaseFirestore.instance
        .collection(FireStoreKey.USERS)
        .where(FireStoreKey.userName, isEqualTo: username)
        .get();

    return snapshot.docs.length > 0;
  }

  Future<bool> checkMail(String? email) async {
    QuerySnapshot snapshot = await FirebaseFirestore.instance
        .collection(FireStoreKey.USERS)
        .where(FireStoreKey.email, isEqualTo: email)
        .get();

    return snapshot.docs.length > 0;
  }

  Future<Users> getUsersById(String userId) async {
    Users user = new Users();
    List<String> friendStrings = [];
    List<String> chatIdStrings = [];
    QuerySnapshot snap = await FirebaseFirestore.instance
        .collection(FireStoreKey.USERS)
        .where(FireStoreKey.userId, isEqualTo: userId)
        .get();

    user.userName = snap.docs[0][FireStoreKey.userName];
    user.imageUrl = snap.docs[0][FireStoreKey.image];
    user.email = snap.docs[0][FireStoreKey.email];
    user.aboutMe = snap.docs[0][FireStoreKey.aboutMe];
    user.userId = snap.docs[0][FireStoreKey.userId];
    List friendList = snap.docs[0][FireStoreKey.friendList];
    List chatIdList = snap.docs[0][FireStoreKey.chatRoomIDs];
    friendList.forEach((a) {
      friendStrings.add(a);
    });
    chatIdList.forEach((b) {
      chatIdStrings.add(b);
    });
    user.friendList = friendStrings;
    user.chatRoomIDs = chatIdStrings;
    user.documentId = snap.docs[0].id;

    return user;
  }

  Future<List<DocumentSnapshot>> friendSearchList(
      String? username, String? username2, List<String>? friendId) async {
    List<DocumentSnapshot> data = [];
    List<DocumentSnapshot> data2 = [];
    List<DocumentSnapshot> data3 = [];

    try {
      await FirebaseFirestore.instance
          .collection(FireStoreKey.USERS)
          .where(FireStoreKey.userName, isEqualTo: username)
          .where(FireStoreKey.userName, isNotEqualTo: username2)
          .get()
          .then((result) {
        data = result.docs;
      }).catchError((e) {
        throw e;
      });

      for (int i = 0; i < data.length; i++) {
        for (int x = 0; x < friendId!.length; x++) {
          if (data[i][FireStoreKey.userId] == friendId[x]) {
            data2.add(data[i]);
          }
        }
      }
      data.forEach((element) {
        if (!data2.contains(element)) {
          data3.add(element);
        }
      });
    } catch (e) {
      rethrow;
    }

    return data3;
  }

  Future<QuerySnapshot> getUserByEmail(String email) async {
    QuerySnapshot snapshot = await FirebaseFirestore.instance
        .collection(FireStoreKey.USERS)
        .where(FireStoreKey.email, isEqualTo: email)
        .get();

    return snapshot;
  }

  addFriends(Users users, FriendsList friend, FriendsList user) async {
    try {
      FirebaseFirestore.instance
          .collection(FireStoreKey.FRIENDS)
          .doc(FireStoreKey.friendList + '_' + users.userId!)
          .collection(FireStoreKey.FRIENDLISTS)
          .add(friend.toMap())
          .catchError((e) => throw e);

      FirebaseFirestore.instance
          .collection(FireStoreKey.FRIENDS)
          .doc(FireStoreKey.friendList + '_' + friend.userId!)
          .collection(FireStoreKey.FRIENDLISTS)
          .add(user.toMap())
          .catchError((e) => throw e);

      FirebaseFirestore.instance
          .collection(FireStoreKey.USERS)
          .doc(users.documentId)
          .update({
        FireStoreKey.friendList: FieldValue.arrayUnion([friend.userId])
      }).catchError((e) => throw e);

      FirebaseFirestore.instance
          .collection(FireStoreKey.USERS)
          .doc(friend.documentId)
          .update({
        FireStoreKey.friendList: FieldValue.arrayUnion([users.userId])
      }).catchError((e) => throw e);
    } catch (e) {
      throw e;
    }
  }

  updateChatRoomList(String? roomId, String? userDocId, String? frDocId) async {
    try {
      FirebaseFirestore.instance
          .collection(FireStoreKey.USERS)
          .doc(userDocId)
          .update({
        FireStoreKey.chatRoomIDs: FieldValue.arrayUnion([roomId])
      }).catchError((e) => throw e);

      FirebaseFirestore.instance
          .collection(FireStoreKey.USERS)
          .doc(frDocId)
          .update({
        FireStoreKey.chatRoomIDs: FieldValue.arrayUnion([roomId])
      }).catchError((e) => throw e);
    } catch (e) {
      throw e.toString();
    }
  }

  createChatRoom(String chatRoomId, chatRoomMap) {
    FirebaseFirestore.instance
        .collection(FireStoreKey.CHATROOM)
        .doc(chatRoomId)
        .set(chatRoomMap)
        .catchError((e) {
      print(e.toString());
    });
  }

  createInRoomStatus(String? chatroomId, String? userId, String? friendId) {
    Map<String, dynamic> data = {FireStoreKey.isInRoom: true};
    Map<String, dynamic> data2 = {FireStoreKey.isInRoom: false};
    FirebaseFirestore.instance
        .collection(FireStoreKey.CHATROOM)
        .doc(chatroomId)
        .collection(FireStoreKey.INROOMSTATS)
        .doc(userId)
        .set(data);

    FirebaseFirestore.instance
        .collection(FireStoreKey.CHATROOM)
        .doc(chatroomId)
        .collection(FireStoreKey.INROOMSTATS)
        .doc(friendId)
        .set(data2);
  }

  updateInRoom(String? chatroomid, String? userId, bool isInRoom) {
    Map<String, dynamic> data = {FireStoreKey.isInRoom: isInRoom};
    FirebaseFirestore.instance
        .collection(FireStoreKey.CHATROOM)
        .doc(chatroomid)
        .collection(FireStoreKey.INROOMSTATS)
        .doc(userId)
        .set(data);
  }

  updateIsRead(String? chatroomid, String? friendName) async {
    CollectionReference snap2 = FirebaseFirestore.instance
        .collection(FireStoreKey.CHATROOM)
        .doc(chatroomid)
        .collection(FireStoreKey.CHATS);

    QuerySnapshot eventsQuery = await snap2
        .where(FireStoreKey.isRead, isEqualTo: false)
        .where(FireStoreKey.sendBy, isEqualTo: friendName)
        .get();

    if (eventsQuery.docs.length > 0) {
      try {
        eventsQuery.docs.forEach((msgDoc) {
          msgDoc.reference.update({FireStoreKey.isRead: true});
        });
      } catch (e) {
        print(e.toString());
      }
    } else {
      return;
    }
  }

  updateLastChat(String? chatroomid, String? lastChats, String? chatType) {
    Map<String, dynamic> lastChat = {
      FireStoreKey.lastChat: lastChats,
      FireStoreKey.lastChatType: chatType
    };
    FirebaseFirestore.instance
        .collection(FireStoreKey.CHATROOM)
        .doc(chatroomid)
        .update(lastChat);
  }

  Future<Stream<QuerySnapshot>> getFriendList(
      String? userId, String val) async {
    return FirebaseFirestore.instance
        .collection(FireStoreKey.FRIENDS)
        .doc(FireStoreKey.friendList + '_' + userId!)
        .collection(FireStoreKey.FRIENDLISTS)
        .where(FireStoreKey.nameSearch, arrayContains: val)
        .snapshots();
  }

  createChatPeers(String? chatRoomId, ChatPeers user, ChatPeers friend) {
    FirebaseFirestore.instance
        .collection(FireStoreKey.CHATPEERS)
        .doc(user.userId)
        .collection(FireStoreKey.peers)
        .doc(friend.userId)
        .set(friend.toMap());

    FirebaseFirestore.instance
        .collection(FireStoreKey.CHATPEERS)
        .doc(friend.userId)
        .collection(FireStoreKey.peers)
        .doc(user.userId)
        .set(user.toMap());
  }

  getChatPeers(Users user, String? val) {
    return FirebaseFirestore.instance
        .collection(FireStoreKey.CHATPEERS)
        .doc(user.userId)
        .collection(FireStoreKey.peers)
        .where(FireStoreKey.nameSearch, arrayContains: val)
        .snapshots();
  }

  updateChatPeers(
    Users user,
    FriendsList friend,
    String? lastChat,
    String? lastType,
    String? imagesContent,
  ) {
    Map<String, dynamic> map = {
      FireStoreKey.lastChat: lastChat,
      FireStoreKey.lastChatType: lastType,
      FireStoreKey.imagesContent: imagesContent
    };

    try {
      FirebaseFirestore.instance
          .collection(FireStoreKey.CHATPEERS)
          .doc(user.userId)
          .collection(FireStoreKey.peers)
          .doc(friend.userId)
          .update(map)
          .catchError((e) => throw e);

      FirebaseFirestore.instance
          .collection(FireStoreKey.CHATPEERS)
          .doc(friend.userId)
          .collection(FireStoreKey.peers)
          .doc(user.userId)
          .update(map)
          .catchError((e) => throw e);
    } catch (e) {
      throw e.toString();
    }
  }

  addConversationMessage(String chatroomId, messageMap) {
    FirebaseFirestore.instance
        .collection(FireStoreKey.CHATROOM)
        .doc(chatroomId)
        .collection(FireStoreKey.CHATS)
        .add(messageMap)
        .catchError((e) {});
  }

  Future<Stream<QuerySnapshot>> getConversationMessage(
      String chatroomId) async {
    return FirebaseFirestore.instance
        .collection(FireStoreKey.CHATROOM)
        .doc(chatroomId)
        .collection(FireStoreKey.CHATS)
        .orderBy(FireStoreKey.chatTime, descending: true)
        .snapshots();
  }

  Stream<DocumentSnapshot> getInRoomStatus(String chatroomId, String friendId) {
    return FirebaseFirestore.instance
        .collection(FireStoreKey.CHATROOM)
        .doc(chatroomId)
        .collection(FireStoreKey.INROOMSTATS)
        .doc(friendId)
        .snapshots();
  }

  getChatRoomId(String? a, String? b) {
    if (a!.substring(0, 1).codeUnitAt(0) > b!.substring(0, 1).codeUnitAt(0)) {
      return "$b\_$a";
    } else {
      return "$a\_$b";
    }
  }
}
