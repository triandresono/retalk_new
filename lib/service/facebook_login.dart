import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:retalk/models/users.dart';

class FacebookLogin {
  static Future<Users> fbLogin() async {
    Users users = new Users();
    await FacebookAuth.instance.login(
        permissions: ['public_profile', 'email']).catchError((e) => throw e);

    Map data = await FacebookAuth.instance.getUserData();

    users.email = data['email'];
    users.userName = data['name'];
    users.imageUrl = data['picture']['data']['url'];
    users.password = data['id'];

    return users;
  }

  static Future fbLogOut() async {
    await FacebookAuth.instance.logOut();
  }
}
