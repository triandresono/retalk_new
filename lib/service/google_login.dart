import 'package:google_sign_in/google_sign_in.dart';
import 'package:retalk/models/users.dart';

class GoogleLogin {
  static Future<Users> googleLogin() async {
    GoogleSignIn login = new GoogleSignIn();
    Users user = new Users();
    GoogleSignInAccount? result = await login.signIn();

    user.imageUrl = result!.photoUrl;
    user.userName = result.displayName;
    user.email = result.email;
    user.password = result.id;

    return user;
  }

  static Future googleLogout() async {
    GoogleSignIn login = new GoogleSignIn();

    await login.signOut();
  }
}
