import 'package:retalk/models/users.dart';
import 'package:retalk/screens/helper/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DatabaseMethods {
  Future saveUser({required Users user}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    await prefs.setString(DatabaseKey.userId, user.userId!);
    await prefs.setString(DatabaseKey.email, user.email!);
    await prefs.setString(DatabaseKey.userName, user.userName!);
    await prefs.setString(DatabaseKey.aboutMe, user.aboutMe!);
    await prefs.setString(DatabaseKey.imageUrl, user.imageUrl!);
    await prefs.setString(DatabaseKey.documentId, user.documentId!);
    await prefs.setStringList(DatabaseKey.friendList, user.friendList!);
    await prefs.setStringList(DatabaseKey.chatRoomIDs, user.chatRoomIDs!);
  }

  Future<Users> getUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Users user = new Users();

    user.userId = prefs.getString(DatabaseKey.userId);
    user.email = prefs.getString(DatabaseKey.email);
    user.userName = prefs.getString(DatabaseKey.userName);
    user.aboutMe = prefs.getString(DatabaseKey.aboutMe);
    user.imageUrl = prefs.getString(DatabaseKey.imageUrl);
    user.documentId = prefs.getString(DatabaseKey.documentId);
    user.friendList = prefs.getStringList(DatabaseKey.friendList);
    user.chatRoomIDs = prefs.getStringList(DatabaseKey.chatRoomIDs);

    return user;
  }

  Future dbClear() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.clear();
  }

  Future notifyLoggedin({bool? isLoggedIn}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return await prefs.setBool(DatabaseKey.isLoggedIn, isLoggedIn!);
  }

  static Future<bool> getUserLoggedInSharedPreference() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(DatabaseKey.isLoggedIn)!;
  }

  Future<List<String>> getFriends() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> friendList = prefs.getStringList(DatabaseKey.friendList)!;
    return friendList;
  }

  Future updateFriend(String? friendId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String>? friendList = prefs.getStringList(DatabaseKey.friendList);
    friendList!.add(friendId!);
    await prefs.setStringList(DatabaseKey.friendList, friendList);
  }

  Future<List<String>> getChatRooms() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> chatRooms = prefs.getStringList(DatabaseKey.chatRoomIDs)!;
    return chatRooms;
  }

  Future updateChatRoomList(String chatroomId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> chatRooms = prefs.getStringList(DatabaseKey.chatRoomIDs)!;
    chatRooms.add(chatroomId);
    await prefs.setStringList(DatabaseKey.chatRoomIDs, chatRooms);
  }
}
