import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:retalk/bloc/chat_room_bloc/chat_room_bloc.dart';
import 'package:retalk/bloc/friendlist_bloc/friendlist_bloc.dart';
import 'package:retalk/models/friends.dart';
import 'package:retalk/models/users.dart';
import 'package:retalk/screens/chat_screen/screens/chat_room.dart';
import 'package:retalk/screens/chat_screen/screens/friends_screen.dart';
import 'package:retalk/screens/chat_screen/screens/home_page.dart';
import 'package:retalk/screens/helper/constants.dart';
import 'package:retalk/screens/login_screen/login_main.dart';
import 'package:retalk/screens/profile_screen/profile_screen.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    try {
      switch (settings.name) {
        case PageConstants.LOGIN_MAIN:
          return CupertinoPageRoute(
              settings: RouteSettings(name: settings.name),
              builder: (_) => LoginMain());
        case PageConstants.HOME_PAGE:
          final Users args = settings.arguments as Users;
          return CupertinoPageRoute(
              settings: RouteSettings(name: settings.name),
              builder: (_) => HomePage(user: args));
        case PageConstants.USER_PROFILE:
          final Users args = settings.arguments as Users;
          return CupertinoPageRoute(
              settings: RouteSettings(name: settings.name),
              builder: (_) => ProfileScreen(user: args));
        case PageConstants.USER_PROFILE:
          final Users args = settings.arguments as Users;
          return CupertinoPageRoute(
              settings: RouteSettings(name: settings.name),
              builder: (_) => ProfileScreen(user: args));
        case PageConstants.FRIEND_LIST:
          return CupertinoPageRoute(
              settings: RouteSettings(name: settings.name),
              builder: (_) => BlocProvider(
                  create: (context) {
                    return FriendListBloc();
                  },
                  child: FriendsScreen()));
        case PageConstants.CHAT_ROOM:
          final FriendsList args = settings.arguments as FriendsList;
          return CupertinoPageRoute(
              settings: RouteSettings(name: settings.name),
              builder: (_) => BlocProvider(
                  create: (context) {
                    return ChatRoomBloc();
                  },
                  child: ChatRoom(data: args)));
        default:
          return _errorRoute();
      }
    } catch (e) {
      return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    AppBar appBar() {
      return AppBar(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(30),
          ),
        ),
        elevation: 3,
        backgroundColor: Color.fromRGBO(245, 245, 245, 1),
        titleSpacing: 1,
        automaticallyImplyLeading: false,
        title: Stack(
          children: <Widget>[
            Row(
              children: [
                Container(
                  margin: EdgeInsets.only(right: 10),
                  height: 40,
                  width: 40,
                  child: Center(
                      child: GestureDetector(
                    behavior: HitTestBehavior.opaque,
                    onTap: () {},
                    child: Container(
                      child: Icon(Icons.replay),
                    ),
                  )),
                ),
                Text(
                  "Error",
                  style: TextStyle(
                      color: Color.fromRGBO(42, 68, 85, 1),
                      fontWeight: FontWeight.bold,
                      fontSize: 20),
                ),
              ],
            ),
          ],
        ),
      );
    }

    return MaterialPageRoute(
        settings: RouteSettings(name: 'Error'),
        builder: (_) {
          return Scaffold(
            appBar: appBar(),
            body: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text('Reload'),
                  Container(
                    margin: EdgeInsets.all(4),
                  ),
                  GestureDetector(
                    behavior: HitTestBehavior.opaque,
                    onTap: () {},
                    child: Container(
                      child: Icon(Icons.replay),
                    ),
                  )
                ],
              ),
            ),
          );
        });
  }
}
