class ChatPeers {
  String? userId;
  String? userName;
  String? email;
  String? aboutMe;
  String? imageUrl;
  String? documentId;
  String? imagesContent;
  String? lastChat;
  String? lastChatType;

  List<String>? nameSearch;
  List<String>? emailSearch;

  //helper
  String? password;
  String? chatRoomId;

  ChatPeers({
    this.aboutMe,
    this.email,
    this.imageUrl,
    this.userId,
    this.userName,
    this.password,
    this.nameSearch,
    this.emailSearch,
    this.chatRoomId,
    this.lastChat,
    this.imagesContent,
    this.lastChatType,
    this.documentId,
  });

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["userId"] = userId;
    map["userName"] = userName;
    map["email"] = email;
    map["aboutMe"] = aboutMe;
    map["imageUrl"] = imageUrl;
    map["documentId"] = documentId;
    map["imagesContent"] = imagesContent;
    map["lastChats"] = lastChat;
    map["lastChatType"] = lastChatType;
    map["chatRoomId"] = chatRoomId;
    map["nameSearch"] = nameSearch;
    map["emailSearch"] = emailSearch;
    return map;
  }
}
