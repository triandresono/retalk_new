class FriendsList {
  String? userId;
  String? userName;
  String? email;
  String? aboutMe;
  String? imageUrl;
  String? documentId;

  List<String>? nameSearch;
  List<String>? emailSearch;

  //helper
  String? password;
  String? chatRoomId;

  FriendsList({
    this.aboutMe,
    this.email,
    this.imageUrl,
    this.userId,
    this.userName,
    this.password,
    this.nameSearch,
    this.emailSearch,
    this.chatRoomId,
    this.documentId,
  });

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["userId"] = userId;
    map["userName"] = userName;
    map["email"] = email;
    map["aboutMe"] = aboutMe;
    map["imageUrl"] = imageUrl;
    map["documentId"] = documentId;
    map["nameSearch"] = nameSearch;
    map["emailSearch"] = emailSearch;
    return map;
  }
}
