class Users {
  String? userId;
  String? userName;
  String? email;
  String? aboutMe;
  String? imageUrl;
  String? documentId;
  List<String>? friendList;
  List<String>? blockList;
  List<String>? chatRoomIDs;

  //helper
  String? password;

  Users({
    this.aboutMe,
    this.email,
    this.imageUrl,
    this.userId,
    this.userName,
    this.password,
    this.friendList,
    this.blockList,
    this.chatRoomIDs,
    this.documentId,
  });

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["userId"] = userId;
    map["userName"] = userName;
    map["email"] = email;
    map["aboutMe"] = aboutMe;
    map["imageUrl"] = imageUrl;
    map["documentId"] = documentId;
    map["friendList"] = friendList;
    map["blockList"] = blockList;
    map["chatRoomIDs"] = chatRoomIDs;
    return map;
  }
}
