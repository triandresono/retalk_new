import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:retalk/screens/login_screen/login_main.dart';
// ignore: unused_import
import 'package:retalk/screens/splash_screens/splash_screens.dart';
import 'package:retalk/service/locator.dart';
import 'package:retalk/service/navigator_service.dart';
import 'package:retalk/service/route_generator.dart';

void main() async {
  setupLocator();
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      navigatorKey: locator<NavigatorService>().navigatorKey,
      title: 'Retalk',
      theme: ThemeData(
        fontFamily: 'OpenSans',
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: LoginMain(),
      onGenerateRoute: (settings) => RouteGenerator.generateRoute(settings),
    );
  }
}
